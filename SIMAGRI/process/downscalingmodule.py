# -*- coding: utf-8 -*-
"""
Updated by Eunjin Han @IRI on October, 2020 by fully implementing Pytyon-based WGEN
Developed by Eunjin Han @IRI on March, 2020
=====================================
##Program: Temporal disaggregation of tercile seasonal climate forecats
##  A Hybrid method by combining Resampling and Richardson's WGEN
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
# Structure is based on the CAMDT-PEST by Seongkyu Lee, APEC Climate Center
##===================================================================
"""
from SIMAGRI._compact import *
from SIMAGRI.process.errormessages import ModuleErrorMessage
from SIMAGRI.process.WGEN_main import WGEN_generator
from SIMAGRI.process.WGEN_PAR import WGEN_PAR_biweekly
from SIMAGRI.process.low_freq_correction import low_freq_correction
from SIMAGRI.process.get_index_sortedYR import get_ind_sorted_yr
from SIMAGRI.process.write_WTH_ensemble import write_WTH_ensemble
from SIMAGRI.process.write_WTH_ensemble import write_WTH_obs_ensemble
from SIMAGRI.process.write_WTH_ensemble import write_WTH_obs

class DOWNSCALING_PROC:

    _UIParent = None
    _Setting = None

    _ModErrMsg = None

    # _Param_Fkile_Prefix = "param_disease_"

    def __init__(self, setting, uiparanet):
        self._Setting = setting
        self._UIParent = uiparanet

        self._ModErrMsg = ModuleErrorMessage(uiparanet)

    def CREATE_WTH(self):
        start_time = time.process_time() 
        #===================================================
        #Input from GUI
        #===================================================
        WSTA = self._Setting.DSSATSetup1.WStation.getvalue()[0][0:4]   #station name
        #==== header info for WTH file
        LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA) 
        # REFHT = -99.0
        # WNDHT = -99.0

        # Wdir_path = self._Setting.ScenariosSetup.Working_directory

        #convert year-month-date to DOY
        plt_year = self._Setting.DSSATSetup1.plt_year.getvalue()
        temp = plt_year+'-'+self._Setting.DSSATSetup1.plt_month.getvalue()+'-'+self._Setting.DSSATSetup1.plt_date.getvalue() 
        plt_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday  
        plt_date = int(plt_year + repr(plt_doy).zfill(3))  #integer  #e.g. 2015310   # September 15, 2015   ********
        hv_date = plt_date + 210 #tentative harvest date => long enough considering delayed growth
        
        #The first date of SCF season => From this date, n realizations of synthetic weather data are written in *.WTH
        #    Before, this date, observed weather data is written in *.WTH
        #This is needed in case of hindcast from which obs weather is available throughout the crop growing season
        #frst_date = 2015335   #******* from which ensemble 100 realizations are attached to the observed
        trimester = self._Setting.DSSATSetup1.trimester1.getvalue()[0]
        frst_date = find_frst_date(plt_date, hv_date, trimester)  #frst_date in format YYYYDOY[int]
        IC_date = plt_date - 1  #for weather generation for IC => IC starts 1 days before the first planting date
        #adjust IC_date if initial conditionn belong to the previous year
        if plt_date%1000 == 1 : #<= 30:
            IC_date = (plt_date//1000 -1)*1000 + 365
            if calendar.isleap(plt_date//1000 -1):  IC_date = (plt_date//1000 -1)*1000 + 366

        #adjust hv_date if harvest moves to a next year
        yr_end_date = 365
        if calendar.isleap(plt_date//1000): yr_end_date = 366
        if hv_date%1000 > yr_end_date:
            hv_date = hv_date - yr_end_date + 1000

        #================== MAIN PROGRAM ======================================
        target_year = int(plt_year)  #2016
        nsample = 500 #===> number of years sampled to generate parameters for WGEN
        nrealiz = 100 #number of years to generate (output of WGEN)
        year2_flag = 0 # 0 means generating weather realizations for only 1 yrs, 1 means generating for 2 years because growing season might go beyond end of year1
        # => call WGEN_main
        wdir = self._Setting.ScenariosSetup.Working_directory
        print( '==START WEATHER GENERATION PROCEDURES')
        print( '==READ HISTORICAL WEATHER OBSERVATIONS (WTD) FILE')
        WTD_fname = path.join(self._Setting.ScenariosSetup.Working_directory, WSTA + ".WTD")
        # === Read daily observations into a dataframe (note: Feb 29th was skipped in df_obs)
        WTD_df_orig, df_obs = read_WTD(wdir, WTD_fname)

        #make df_doy  EJ(10/15/2020)
        m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]
        m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365]
        numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]

        df_doy = pd.DataFrame(np.zeros((365, 2)))   
        df_doy.columns = ['MONTH','DOY']  #works only for regular years, not leap years
        for i in range(12):
            a = m_doys_list[i]-1
            b = m_doye_list[i]
            df_doy.iloc[a:b,0]=np.tile(i+1,(numday_list[i],))
            df_doy.iloc[a:b,1]= np.arange(m_doys_list[i],m_doye_list[i]+1)
        #====================================================================

        #EJ(2/21/2020)
        #read SCF for two consecutive years from the GUI
        frst_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "SCF_input_2yrs.csv") 
        df_scf = pd.read_csv(frst_fname, header = 0, index_col ="Month") #read *.csv data into a dataframe
        temp = df_scf.loc["SCF"].values
        SCF1_loc = np.where(temp == 1) #SCF1 
        SCF2_loc = np.where(temp == 2) #SCF2

        #see case lists:https://docs.google.com/spreadsheets/d/1XnBcHzbTDtDJpHl9U9SX0AfNgDjy4Vci/edit#gid=801540749
        #=========================================================
        #case #1: both SCF1 and SCF2 are in YR1
        #=========================================================
        if SCF1_loc[0][0] > 0 and SCF1_loc[0][0] < 6:
            sdoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+1].DOY.values[0] #first day of the first month of the given season
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            sdoy = int(sdoy)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = edoy + 1
            end_month = SCF2_loc[0][-1] + 1
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)
            # count, bins, ignored = plt.hist(SCF1_yr_list, nyears, density=True)
            # plt.title('SCF1: 50BN, 30AN')
            # plt.show()
            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)
            # count, bins, ignored = plt.hist(SCF2_yr_list, nyears, density=True)
            # plt.title('SCF2: 40BN, 25AN')
            # plt.show()
            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 
            # count, bins, ignored = plt.hist(clim_yr_list, nyears, density=True)
            # plt.title('climatology')
            # plt.show()
            #========================================================
            print ("Resampling histrical years before WGEN-PAR. Please wait....")
            #====4) Aggregate daily weather data based on the resampled year list
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,0:sdoy-1] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                srad_resampled[i,0:sdoy-1] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmin_resampled[i,0:sdoy-1] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmax_resampled[i,0:sdoy-1] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                #b)SCF1
                rain_resampled[i,sdoy-1:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,sdoy-1:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,sdoy-1:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,sdoy-1:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                #c)SCF2
                rain_resampled[i,sdoy2-1:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,sdoy2-1:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,sdoy2-1:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,sdoy2-1:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                #d)climatology
                rain_resampled[i,edoy2:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                srad_resampled[i,edoy2:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmin_resampled[i,edoy2:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmax_resampled[i,edoy2:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values

            print("It took {0:5.1f}, sec to finish resampling years". format(time.process_time() - start_time)) 
            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year, rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters, WGEN_PAR_biweekly". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            # print("It took {0:5.1f}, sec to finish calling WGEN_generator". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            # print("It took {0:5.1f}, sec to finish calling low frequency correction". format(time.process_time() - start_time)) 

        #=========================================================
        #case #2: both SCF1 and SCF2 are in YR1 but SCF2 is for OND. Therefore, Year 2 is fully based on climatology
        #=========================================================
        elif SCF1_loc[0][0] == 6:
            sdoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+1].DOY.values[0] #first day of the first month of the given season
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            sdoy = int(sdoy)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = edoy + 1
            end_month = SCF2_loc[0][-1] + 1
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)

            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)

            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 

            #========================================================
            #====4) Aggregate daily weather data based on the resampled year list
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,0:sdoy-1] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                srad_resampled[i,0:sdoy-1] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmin_resampled[i,0:sdoy-1] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmax_resampled[i,0:sdoy-1] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                #b)SCF1
                rain_resampled[i,sdoy-1:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,sdoy-1:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,sdoy-1:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,sdoy-1:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                #c)SCF2
                rain_resampled[i,sdoy2-1:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,sdoy2-1:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,sdoy2-1:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,sdoy2-1:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values

            print("It took {0:5.1f}, sec to finish resampling years". format(time.process_time() - start_time)) 
            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year, rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters, WGEN_PAR_biweekly". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction". format(time.process_time() - start_time)) 

            if year2_flag == 1: #make Year 2 weather data based on climatology
                rain_resampled = np.empty((nsample,365,))*np.NAN 
                srad_resampled = np.empty((nsample,365,))*np.NAN 
                tmin_resampled = np.empty((nsample,365,))*np.NAN 
                tmax_resampled = np.empty((nsample,365,))*np.NAN 
                for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
                # for i in range(nsample):
                    rain_resampled[i,:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])].values
                    srad_resampled[i,:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])].values
                    tmin_resampled[i,:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])].values
                    tmax_resampled[i,:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])].values
                #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
                WGEN_PAR_biweekly(wdir, target_year+1,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
                print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #2)". format(time.process_time() - start_time)) 
                #Generate daily weather realizations based on the estimated parameters
                WGEN_generator(wdir, target_year+1,nrealiz)
                print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #2)". format(time.process_time() - start_time)) 
                #Correct low frequency (inter-annual variability of monthly weather data)
                df_WGEN_corr = low_freq_correction(wdir, target_year+1) 
                print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #2)". format(time.process_time() - start_time)) 
        #=========================================================
        # #Case #3: SCF1 is for ASO or SON
        #=========================================================
        elif SCF1_loc[0][0] == 7 or SCF1_loc[0][0] == 8: 
            sdoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+1].DOY.values[0] #first day of the first month of the given season
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            sdoy = int(sdoy)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = edoy + 1
            end_month = SCF2_loc[0][-1] + 1 -12
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)

            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)

            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 

            #========================================================
            #====4) Aggregate daily weather data based on the resampled year list FOR TWO YEARS
            #======================> (1) YEAR #1
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,0:sdoy-1] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                srad_resampled[i,0:sdoy-1] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmin_resampled[i,0:sdoy-1] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmax_resampled[i,0:sdoy-1] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                #b)SCF1
                rain_resampled[i,sdoy-1:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,sdoy-1:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,sdoy-1:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,sdoy-1:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                #c)SCF2
                rain_resampled[i,edoy:] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= 365)].values
                srad_resampled[i,edoy:] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= 365)].values
                tmin_resampled[i,edoy:] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= 365)].values
                tmax_resampled[i,edoy:] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= 365)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #1)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #1)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #1)". format(time.process_time() - start_time)) 
            #======================> (2) YEAR #2
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)SCF2
                rain_resampled[i,0:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,0:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,0:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,0:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                #b)climatology
                rain_resampled[i,edoy2:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                srad_resampled[i,edoy2:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmin_resampled[i,edoy2:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmax_resampled[i,edoy2:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year+1,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #2)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year+1,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #2)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year+1) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #2)". format(time.process_time() - start_time)) 

        #=========================================================
        # Case #4:SCF1 is for OND, SCF2 is for JFM in next year
        #=========================================================
        elif SCF1_loc[0][0] == 9:
            sdoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+1].DOY.values[0] #first day of the first month of the given season
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            sdoy = int(sdoy)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = 1
            end_month = 3
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)

            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)

            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 

            #========================================================
            #====4) Aggregate daily weather data based on the resampled year list FOR TWO YEARS
            #======================> (1) YEAR #1
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,0:sdoy-1] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                srad_resampled[i,0:sdoy-1] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmin_resampled[i,0:sdoy-1] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmax_resampled[i,0:sdoy-1] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                #b)SCF1
                rain_resampled[i,sdoy-1:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,sdoy-1:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,sdoy-1:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,sdoy-1:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= edoy)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #1)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #1)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #1)". format(time.process_time() - start_time))
            #======================> (2) YEAR #2
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)SCF2
                rain_resampled[i,0:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,0:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,0:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,0:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy2)].values
                #b)climatology
                rain_resampled[i,edoy2:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                srad_resampled[i,edoy2:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmin_resampled[i,edoy2:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmax_resampled[i,edoy2:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year+1,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #2)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year+1,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #2)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year+1) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #2)". format(time.process_time() - start_time))

        #=========================================================
        # Case #5:SCF1 is for NDJ or DJF (SCF2 is for FMA or MAM in next year)
        #=========================================================
        elif SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11: 
            sdoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+1].DOY.values[0] #first day of the first month of the given season
            sdoy = int(sdoy)
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3-12].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = edoy + 1
            end_month = SCF2_loc[0][-1] + 1 -12
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)

            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)

            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 

            #========================================================
            #====4) Aggregate daily weather data based on the resampled year list FOR TWO YEARS
            #======================> (1) YEAR #1
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,0:sdoy-1] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                srad_resampled[i,0:sdoy-1] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmin_resampled[i,0:sdoy-1] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                tmax_resampled[i,0:sdoy-1] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] < sdoy)].values
                #b)SCF1
                rain_resampled[i,sdoy-1:] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= 365)].values
                srad_resampled[i,sdoy-1:] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= 365)].values
                tmin_resampled[i,sdoy-1:] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= 365)].values
                tmax_resampled[i,sdoy-1:] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= sdoy) & (df_obs["DOY"] <= 365)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #1)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #1)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #1)". format(time.process_time() - start_time))
            #======================> (2) YEAR #2
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)SCF1
                rain_resampled[i,0:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,0:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,0:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,0:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                #a)SCF2
                rain_resampled[i,sdoy2-1:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,sdoy2-1:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,sdoy2-1:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,sdoy2-1:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                #b)climatology
                rain_resampled[i,edoy2:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                srad_resampled[i,edoy2:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmin_resampled[i,edoy2:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmax_resampled[i,edoy2:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year+1,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #2)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year+1,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #2)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year+1) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #2)". format(time.process_time() - start_time))

        #=========================================================
        # Case #6: SCF1 starts from January in year 2
        #=========================================================
        elif SCF1_loc[0][0] == 12:
            sdoy = 1
            edoy = df_doy[df_doy["MONTH"] == SCF1_loc[0][0]+3-12].DOY.values[-1] #last day of the last month of the given season(i.e.,3 month)
            edoy = int(edoy)
            df_sorted1 = get_ind_sorted_yr(wdir, df_obs, sdoy, edoy)  #get indices of the sorted years based on SCF1
            sdoy2 = edoy + 1
            end_month = SCF2_loc[0][-1] + 1 -12
            edoy2 = df_doy[df_doy["MONTH"] == end_month].DOY.values[-1]
            edoy2 = int(edoy2)
            df_sorted2 = get_ind_sorted_yr(wdir, df_obs, sdoy2, edoy2) #get indices of the sorted years based on SCF2
            #====1) resampling years for SCF1 period
            BN = df_scf.iloc[0][SCF1_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN = df_scf.iloc[2][SCF1_loc[0][0]]     #e.g., 31%
            SCF1_yr_list = resample_years(df_sorted1,AN,BN,nsample)

            #====2) resampling years for SCF2 period
            BN2 = df_scf.iloc[0][SCF2_loc[0][0]]  #   SCF1_loc[0][0] == 10 or SCF1_loc[0][0] == 11  e.g., 36%
            AN2 = df_scf.iloc[2][SCF2_loc[0][0]]     #e.g., 31%
            SCF2_yr_list = resample_years(df_sorted2,AN2,BN2,nsample)

            #====3) resampling years for climatology
            nyears = len(df_obs.YEAR.unique())
            year1 = df_obs.YEAR.iloc[0].astype(int)  #the first obs year
            year2 = df_obs.YEAR.iloc[-1].astype(int) #the last obs year
            clim_yr_list = np.random.randint(year1, year2+1, nsample) 

            #========================================================
            #====4) Aggregate daily weather data based on the resampled year list FOR TWO YEARS
            #======================> (1) YEAR #1
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)climatology
                rain_resampled[i,:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])].values
                srad_resampled[i,:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])].values
                tmin_resampled[i,:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])].values
                tmax_resampled[i,:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])].values
            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #1)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #1)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #1)". format(time.process_time() - start_time))
            #======================> (2) YEAR #2
            rain_resampled = np.empty((nsample,365,))*np.NAN 
            srad_resampled = np.empty((nsample,365,))*np.NAN 
            tmin_resampled = np.empty((nsample,365,))*np.NAN 
            tmax_resampled = np.empty((nsample,365,))*np.NAN 
            for i in progressbar(range(nsample), "Saving weather variables from sampled years: ", 40):
            # for i in range(nsample):
                #a)SCF1
                rain_resampled[i,0:edoy] = df_obs.RAIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                srad_resampled[i,0:edoy] = df_obs.SRAD[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                tmin_resampled[i,0:edoy] = df_obs.TMIN[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                tmax_resampled[i,0:edoy] = df_obs.TMAX[(df_obs["YEAR"] == SCF1_yr_list[i])& (df_obs["DOY"] >= 1) & (df_obs["DOY"] <= edoy)].values
                #a)SCF2
                rain_resampled[i,sdoy2-1:edoy2] = df_obs.RAIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                srad_resampled[i,sdoy2-1:edoy2] = df_obs.SRAD[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmin_resampled[i,sdoy2-1:edoy2] = df_obs.TMIN[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                tmax_resampled[i,sdoy2-1:edoy2] = df_obs.TMAX[(df_obs["YEAR"] == SCF2_yr_list[i])& (df_obs["DOY"] >= sdoy2) & (df_obs["DOY"] <= edoy2)].values
                #b)climatology
                rain_resampled[i,edoy2:] = df_obs.RAIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                srad_resampled[i,edoy2:] = df_obs.SRAD[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmin_resampled[i,edoy2:] = df_obs.TMIN[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values
                tmax_resampled[i,edoy2:] = df_obs.TMAX[(df_obs["YEAR"] == clim_yr_list[i])& (df_obs["DOY"] > edoy2)].values

            #Estimate parameters for rainfall models and Fourier coefficients for Tmin/Tmax/Srad
            WGEN_PAR_biweekly(wdir, target_year+1,rain_resampled, tmin_resampled, tmax_resampled, srad_resampled)
            print("It took {0:5.1f}, sec to estimate biweekly weather parameters (YEAR #2)". format(time.process_time() - start_time)) 
            #Generate daily weather realizations based on the estimated parameters
            WGEN_generator(wdir, target_year+1,nrealiz)
            print("It took {0:5.1f}, sec to finish calling WGEN_generator (YEAR #2)". format(time.process_time() - start_time)) 
            #Correct low frequency (inter-annual variability of monthly weather data)
            df_WGEN_corr = low_freq_correction(wdir, target_year+1) 
            print("It took {0:5.1f}, sec to finish calling low frequency correction (YEAR #2)". format(time.process_time() - start_time))

        #=================================================================
        #=================================================================
        #Last observed date of the weather data  
        # Note => In case of hindcast, we assume that observed data is available until start of SCF months
        WTD_last_year = WTD_df_orig.YEAR.values[-1] 
        WTD_last_doy = WTD_df_orig.DOY[WTD_df_orig["YEAR"] == WTD_last_year].values[-1]
        WTD_last_date = int(repr(WTD_last_year) + repr(WTD_last_doy))  #e.g., 2017090
        year_array = df_WGEN_corr.iyear.unique()
        # nrealiz = year_array.shape[0]

        #write 100 WTH files for each year
        print( '==Create_WTH: Start to write 100 *.WTH files - please wait...')
        # 1) when only one year is needed (case #5), exclude case #6 (when SCF1 for JFM)
        # if plt_date//1000 == hv_date//1000 and plt_date//1000 == IC_date//1000 and SCF1_loc[0][0] != 12:
        #(a) write WGEN generated weather realizations from the beginning of crop growth (i.e., IC)
        if frst_date <= IC_date: 
            #call a function
            write_WTH_ensemble(IC_date, df_WGEN_corr, wdir, WSTA, LAT, LONG, ELEV, TAV, AMP)
        #(b) write observed weather until frst_date and then WGEN generated weather realizations 
        elif frst_date > IC_date: 
            if WTD_last_date >= frst_date:  
                #write observed weather until frst_date and then WGEN generated weather realizations => Q: why do not use all obs data?
                #This is the typical case of hindcast simulation
                write_WTH_obs_ensemble(IC_date, frst_date, WTD_df_orig, df_WGEN_corr, wdir, WSTA, LAT, LONG, ELEV, TAV, AMP)
            elif WTD_last_date >= IC_date and WTD_last_date < frst_date: 
                #write observed weather until WTD_last_date and then Fill the missing values between WTD_last_date and  IC_date with WGEN-output from climatology
                # print( '==Warning (1): Last observed weather data is between IC of crop simulation (i.e., 1 days before plating) and the start date of SCF')
                # print('Missing days between last observed data and frst_date are filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df_orig, df_WGEN_corr, wdir, WSTA, LAT, LONG, ELEV, TAV, AMP)
            elif WTD_last_date < IC_date: #fill missing days between IC_date and frst_date with WGEN-output from climatology
                # print( '==Warning (2): Last observed weather data is before IC of crop simulation (i.e., 1 days before plating)')
                # print('Missing days between IC_date and frst_date are filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_ensemble(IC_date, df_WGEN_corr, wdir, WSTA, LAT, LONG, ELEV, TAV, AMP)
            else:
                print('No case selected in writing WTH: plt_date//1000 == hv_date//1000 and plt_date//1000 == IC_date//1000')
                os.system('pause')
        # #write obs reference weather data (e.g., SANJ1501.WTH)
        # if WTD_last_date >= hv_date:  #write only when obs data is available for full groiwng period
        #     write_WTH_obs(IC_date, hv_date%1000, 1, WTD_df_orig, wdir, WSTA, LAT, LONG, ELEV, TAV, AMP)

        end_time1 = time.process_time()  # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to write 100 WTH files in Create_WTH.py'.format(end_time1-start_time))
        print( '==End of writing 100 *.WTH files')

        return
#===================================================================
def find_station_info(station_name):
  station_dic = {
    'ASEL': {'LAT': 7.950, 'LONG':39.133, 'ELEV': 2413, 'TAV': 15.4, 'AMP': 3.5},  #Assela
    'AWAS': {'LAT': 7.050, 'LONG':38.467, 'ELEV': 1694, 'TAV': 20.3, 'AMP': 1.9},   #Awassa
    'BAKO': {'LAT': 9.117, 'LONG':37.083, 'ELEV': 1650, 'TAV': 21.0, 'AMP': 3.3},  #Baco
    'KOBO': {'LAT': 12.133, 'LONG':39.633, 'ELEV': 1470, 'TAV': 21.1, 'AMP': 4.7},  #Kobo
    'KULU': {'LAT': 8.017, 'LONG':39.150, 'ELEV': 2211, 'TAV': 19.1, 'AMP': 4.5},  #Kulumsa
    'MAHO': {'LAT': 12.800, 'LONG':39.650, 'ELEV': 1850, 'TAV': 22.3, 'AMP': 4.9},   #Mahoni
    'MEIS': {'LAT': 9.233, 'LONG':40.750, 'ELEV': 1400, 'TAV': 22.2, 'AMP': 5.6},  #Meiso
    'MELK': {'LAT': 8.400, 'LONG':39.317, 'ELEV': 1540, 'TAV': 20.5, 'AMP': 4.1},  #Melkasa
    }
  LAT = station_dic[station_name]['LAT']  #check!
  LONG = station_dic[station_name]['LONG']
  ELEV = station_dic[station_name]['ELEV']
  TAV = station_dic[station_name]['TAV']
  AMP = station_dic[station_name]['AMP']

  return LAT, LONG, ELEV, TAV, AMP 

#======================================================================
# the first date of SCF window
def find_frst_date(plt_date, hv_date, trimester):  
  # #===================================================
  # m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
  # m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
  # #======================================================================
  #define a dictionary to determine the starting date of each trimester
  frst_dic = {"JFM":1,"FMA":32, "MAM":60, "AMJ":91, "MJJ":121, "JJA":152, "JAS":182, "ASO":213, "SON":244, "OND":274, "NDJ":305, "DJF":335}
  frst_dic_leap = {"JFM":1,"FMA":32, "MAM":61, "AMJ":92, "MJJ":122, "JJA":153, "JAS":183, "ASO":214, "SON":245, "OND":275, "NDJ":306, "DJF":336}

  if trimester == 'JFM':  #=> JFM is always in YR2 and thus use harvest date as reference => Case #6
    if calendar.isleap(hv_date//1000):
      temp_doy = frst_dic_leap.get(trimester)
      frst_date = repr(hv_date) + repr(temp_doy).zfill(3)  
    else:
      temp_doy = frst_dic.get(trimester)
      frst_date = repr(plt_date//1000) + repr(temp_doy).zfill(3)  
  else:
    if calendar.isleap(plt_date//1000):
      temp_doy = frst_dic_leap.get(trimester)
      frst_date = repr(plt_date//1000) + repr(temp_doy).zfill(3)  
    else:
      temp_doy = frst_dic.get(trimester)
      frst_date = repr(plt_date//1000) + repr(temp_doy).zfill(3)  

  return int(frst_date)

#====================================================================
# === Read daily observations into a dataframe (note: Feb 29th was skipped in df_obs)
def read_WTD(wdir, fname):
    #1) Read daily observations into a matrix (note: Feb 29th was skipped)
    # WTD_fname = r'C:\Users\Eunjin\IRI\Hybrid_WGEN\CNRA.WTD'
    #1) read observed weather *.WTD (skip 1st row - heading)
    data1 = np.loadtxt(fname,skiprows=1)
    #convert numpy array to dataframe
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
                    'DOY':data1[:,0].astype(int)%1000,
                    'SRAD':data1[:,1],
                    'TMAX':data1[:,2],
                    'TMIN':data1[:,3],
                    'RAIN':data1[:,4]})
    #make a copy of original WTD dataframe 
    WTD_df_orig = WTD_df.copy()
    #=== Extract only years with full 365/366 days:  by checking last obs year if it is incomplete or not
    WTD_last_year = WTD_df.YEAR.values[-1] 
    WTD_last_doy = WTD_df.DOY[WTD_df["YEAR"] == WTD_last_year].values[-1]
    if calendar.isleap(WTD_last_year):
        if WTD_last_doy < 366:
            indexNames = WTD_df[WTD_df["YEAR"] == WTD_last_year].index
            WTD_df.drop(indexNames , inplace=True) # Delete these row indexes from dataFrame
    else:
        if WTD_last_doy < 365:
            indexNames = WTD_df[WTD_df["YEAR"] == WTD_last_year].index
            WTD_df.drop(indexNames , inplace=True)    
    #=== Extract only years with full 365/366 days:  by checking first obs year if it is incomplete or not
    WTD_first_year = WTD_df.YEAR.values[0] 
    WTD_first_date = WTD_df.DOY[WTD_df["YEAR"] == WTD_first_year].values[0]
    if WTD_first_date > 1:
        if calendar.isleap(WTD_first_year):
            indexNames = WTD_df[WTD_df["YEAR"] == WTD_first_year].index
            WTD_df.drop(indexNames , inplace=True)
        else:
            indexNames = WTD_df[WTD_df["YEAR"] == WTD_first_year].index
            WTD_df.drop(indexNames , inplace=True) 
    #========================
    rain_WTD = WTD_df.RAIN.values
    srad_WTD = WTD_df.SRAD.values
    Tmax_WTD = WTD_df.TMAX.values
    Tmin_WTD = WTD_df.TMIN.values
    year_WTD = WTD_df.YEAR.values
    doy_WTD = WTD_df.DOY.values
    obs_yrs = np.unique(year_WTD).shape[0]
    #Exclude Feb. 29th in leapyears
    temp_indx = [1 if (calendar.isleap(year_WTD[i])) & (doy_WTD[i] == 29) else 0 for i in range(len(year_WTD))] #[f(x) if condition else g(x) for x in sequence]
    # Get the index of elements with value 15  result = np.where(arr == 15)
    rain_array = rain_WTD[np.where(np.asarray(temp_indx) == 0)]
    rain_array = np.reshape(rain_array, (obs_yrs,365))
    srad_array = srad_WTD[np.where(np.asarray(temp_indx) == 0)]
    srad_array = np.reshape(srad_array, (obs_yrs,365))
    Tmax_array = Tmax_WTD[np.where(np.asarray(temp_indx) == 0)]
    Tmax_array = np.reshape(Tmax_array, (obs_yrs,365))
    Tmin_array = Tmin_WTD[np.where(np.asarray(temp_indx) == 0)]
    Tmin_array = np.reshape(Tmin_array, (obs_yrs,365))

    #save dataframe into a csv file [Note: Feb 29th was excluded]
    df_obs = pd.DataFrame(np.zeros((obs_yrs*365, 6)))   
    df_obs.columns = ['YEAR','DOY','SRAD','TMAX','TMIN','RAIN']  #iyear => ith year
    df_obs.name = 'WTD_observed_365'
    k = 0
    for i in range(obs_yrs):
        iyear = np.unique(year_WTD)[i]
        df_obs.YEAR.iloc[k:365*(i+1)] = np.tile(iyear,(365,))
        df_obs.DOY.iloc[k:365*(i+1)]= np.asarray(range(1,366))
        df_obs.SRAD.iloc[k:365*(i+1)]= np.transpose(srad_array[i,:])
        df_obs.TMAX.iloc[k:365*(i+1)]= np.transpose(Tmax_array[i,:])
        df_obs.TMIN.iloc[k:365*(i+1)]= np.transpose(Tmin_array[i,:])
        df_obs.RAIN.iloc[k:365*(i+1)]= np.transpose(rain_array[i,:])
        k=k+365
    #write dataframe into CSV file
    df_obs.to_csv(wdir +'//'+ df_obs.name + '.csv', index=False)
    del rain_WTD; del srad_WTD; del Tmax_WTD; del Tmin_WTD; del year_WTD; del doy_WTD
    del rain_array; del srad_array; del Tmax_array; del Tmin_array
    return WTD_df_orig, df_obs
#====================================================================
# End of reading observations (WTD file) into a matrix 
#====================================================================
def resample_years(df_sorted,AN,BN,nsample):
    nyears = len(df_sorted.YEAR.values)

    gnum_BN = int(nsample * (BN/100.0))  #generated numbers for BN
    gnum_AN = int(nsample * (AN/100.0))
    gnum_NN = nsample - gnum_BN - gnum_AN

    nsample_BN = nyears//3  
    nsample_AN = nyears//3
    nsample_NN = nyears - 2*nsample_BN

    BN_index = np.random.randint(0, nsample_BN, gnum_BN)   #0~9
    #e.g., d1 = np.random.randint(1, 7, 1000) => Random integers of type np.int_ between 1 and 7 (7 is not iuncluded)
    NN_index = nsample_BN + np.random.randint(0, nsample_NN, gnum_NN) #10~19
    AN_index = nsample_BN + nsample_NN + np.random.randint(0, nsample_AN, gnum_AN)  #20~29 for 30 yrs of observations

    BN_yr_list = np.asarray([df_sorted.YEAR.iloc[BN_index[i]] for i in range(gnum_BN)]) #[f(x) if condition else g(x) for x in sequence]
    NN_yr_list = np.asarray([df_sorted.YEAR.iloc[NN_index[i]] for i in range(gnum_NN)])
    AN_yr_list = np.asarray([df_sorted.YEAR.iloc[AN_index[i]] for i in range(gnum_AN)])

    SCF_yr_list = np.concatenate((BN_yr_list.astype(int), NN_yr_list.astype(int)), axis=0)
    SCF_yr_list = np.concatenate((SCF_yr_list, AN_yr_list.astype(int)), axis=0)

    return SCF_yr_list
#======================================================================
def progressbar(it, prefix="", size=60, file=sys.stdout):
    #https://stackoverflow.com/questions/3160699/python-progress-bar
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, count))
        file.flush()        
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()