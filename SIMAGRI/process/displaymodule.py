# -*- coding: utf-8 -*-

from SIMAGRI._compact import *
from SIMAGRI.process.errormessages import ModuleErrorMessage
import SIMAGRI.process.simagri_processes as simagri_procs


class DISPLAY_PROC:

  _UIParent = None
  _Setting = None

  # _Param__File_Prefix = "param_"

  _ModErrMsg = None

  def __init__(self, setting, uiparanet):
    self._Setting = setting
    self._UIParent = uiparanet

    self._ModErrMsg = ModuleErrorMessage(uiparanet)

  def Yield_boxplot(self,fname, scename): #, cr_list):
    count = len(scename)
    n_year = 100
    yield_n = np.empty([n_year,count])*np.nan     # For Boxplot

    #Read Summary.out from all scenario output
    yield_obs = []
    obs_flag_list = []
    for x in range(0, count):
      df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      if HWAM.shape[0] == 101:  #if last line is from the observed weather
          yield_n[:,x] = HWAM[:-1]
          yield_obs.append(HWAM[-1])
          obs_flag_list.append(1)
      else:
          yield_n[:,x] = HWAM
          yield_obs.append(np.nan)
          obs_flag_list.append(0)
    yield_n[yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    # yield_n[yield_n < 0]=np.nan
    # print('after filling', yield_n[:,0])
    # #To remove 'nan' from data array
    # #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
    # mask = ~np.isnan(yield_n)
    # filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]

    #X data for plot
    myXList=[i+1 for i in range(count)]

    #Plotting
    fig = plt.figure()
    fig.suptitle('Yield Estimation', fontsize=12, fontweight='bold')

    ax = fig.add_subplot(111)
    ax.set_xlabel('Scenario',fontsize=12)
    ax.set_ylabel('Yield [kg/ha]',fontsize=12)

    if sum(obs_flag_list) >= 1:  #if any of scenario has yield based on observed weather
      # Plot a line between yields with observed weather
      ax.plot(myXList, yield_obs, 'go-')

    ax.boxplot(yield_n,labels=scename, showmeans=True, meanline=True, notch=True) #, bootstrap=10000)
    # Wdir_path = self._Setting.ScenariosSetup.Working_directory
    # fname.append(path.join(Wdir_path, "Yield_boxplot.png")
    # plt.savefig(fig_name)
    plt.show()


  def Yield_boxplot_hist(self,fname, fname_hist, scename):
    count = len(scename)
    n_year = 100
    yield_n = np.empty([n_year,count])*np.nan     # For Boxplot

    #Read Summary.out from all scenario output
    yield_obs = []
    obs_flag_list = []
    for x in range(0, count):
      df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      if HWAM.shape[0] == 101:  #if last line is from the observed weather
          yield_n[:,x] = HWAM[:-1]
          yield_obs.append(HWAM[-1])
          obs_flag_list.append(1)
      else:
          yield_n[:,x] = HWAM
          yield_obs.append(np.nan)
          obs_flag_list.append(0)
    yield_n[yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    yield_hist = np.empty([70,count])*np.nan  #==> 60 is an arbitrary number
    #Read Summary.out from all scenario output
    max_nyear = 1
    for x in range(0, count):
      df_OUT=pd.read_csv(fname_hist[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      nyear = HWAM.shape[0]
      yield_hist[:nyear,x] = HWAM
      if nyear > max_nyear: max_nyear = nyear

    #trim yield_hist
    yield_hist = yield_hist[0:max_nyear,:]
    yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #plot============================================
    #regroup matrix for different planting date
    i_pos = 1
    fig = plt.figure()
    fig.suptitle('Yield estimation in comparison with climatology', fontsize=12, fontweight='bold')
    ax = plt.axes()
  
    for i in range(count):
      a = yield_hist[:,i].tolist() #.reshape((1,obs_years,1))
      b = yield_n[:,i].tolist()  #.reshape((nrealiz,1))
      
      new_list = []
      new_list.append(a)
      new_list.append(b) #=> nested list
      # first boxplot pair
      bp = plt.boxplot(new_list, positions = [i_pos, i_pos+1], widths = 0.6, showmeans=True, meanline=True, notch=True)
      setBoxColors(bp)
      i_pos = i_pos + 3

    #X data for plot
    myXList=[i+0.5 for i in range(1, 3*count, 3)]
    if sum(obs_flag_list) >= 1:  #if any of scenario has yield based on observed weather
      # Plot a line between yields with observed weather
      plt.plot(myXList, yield_obs, 'go-')

    # set axes limits and labels
    plt.xlim(0,3*count)
    ax.set_xticks(myXList)
    ax.set_xticklabels(scename)
    ax.set_xlabel('Scenarios',fontsize=12)
    ax.set_ylabel('Yield [kg/ha]',fontsize=12)
    # draw temporary red and blue lines and use them to create a legend
    hB, = plt.plot([1,1],'b-')
    hR, = plt.plot([1,1],'r-')
    plt.legend((hB, hR),('Historical', 'SCF'), loc='best')
    hB.set_visible(False)
    hR.set_visible(False)
    # fig.set_size_inches(12, 8)
    # fig_name = Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot2.png"
    # plt.savefig(fig_name,dpi=100)   
    plt.show()  


  def Yield_exceedance(self,fname, scename): #, cr_list):
    count = len(scename)
    n_year = 100
    yield_n = np.empty([n_year,count])*np.nan     # For Boxplot

    #Read Summary.out from all scenario output
    yield_obs = []
    obs_flag_list = []
    sorted_yield_n = np.empty([n_year,count])*np.nan
    Fx_scf = np.empty([n_year,count])*np.nan 
    for x in range(0, count):
      df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      if HWAM.shape[0] == 101:  #if last line is from the observed weather
          # yield_n[:,x] = HWAM[:-1]
          yield_obs.append(HWAM[-1])
          obs_flag_list.append(1)
          #to make exceedance curve
          sorted_yield_n[:,x] = np.sort(HWAM[:-1])
          fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
          #Fx_scf = np.cumsum(fx_scf)
          Fx_scf[:,x] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
      else:
          # yield_n[:,x] = HWAM
          yield_obs.append(np.nan)
          obs_flag_list.append(0)
          #to make exceedance curve
          sorted_yield_n[:,x] = np.sort(HWAM)
          fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
          #Fx_scf = np.cumsum(fx_scf)
          Fx_scf[:,x] = 1.0-np.cumsum(fx_scf)  #for exceedance curve

    yield_n[yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    sorted_yield_n[sorted_yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #4) Plotting yield exceedance curve
    fig = plt.figure()
    fig.suptitle('Yield Exceedance Curve', fontsize=12, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Yield [kg/ha]',fontsize=12)
    ax.set_ylabel('Probability of Exceedance [-]',fontsize=12)
    plt.ylim(0, 1) 
    for x in range(count):
      ax.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
      #vertical line for the yield with observed weather
      if obs_flag_list[x] == 1:
        x_data=[yield_obs[x],yield_obs[x]] #only two points to draw a line
        y_data=[0,1]
        temp='w/ obs wth (' + scename[x] + ')'
        ax.plot(x_data,y_data,'-.', label=temp)
        plt.ylim(0, 1)
    box = ax.get_position()  # Shrink current axis by 15%
    ax.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    # fig_name = Wdir_path + "\\" + sname + "_output\\" + sname +"_Yield_exceedance.png"
    # plt.savefig(fig_name)  
    plt.show()


  def WSGD_plot(self,fname, scename, obs_flag_list): #, cr_list):
    count = len(scename)
    n_days = 180 #approximate growing days
    nrealiz = 101 #100 realization + 1 observation case
    WSGD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weahter realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              WSGD_3D[d_count, n_count, x] = float(line[238:243]) #WFGD   H20 factor,gr   Water factor for growth (0-1)  for WHEAT
            else:
              WSGD_3D[d_count, n_count, x] = float(line[127:133])  #WSGD   H20 stress,ex   Water stress - expansion/partioning/development (0-1)
            # WSGD_3D[d_count, n_count, x] = float(line[127:133])
            d_count = d_count + 1

    # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN    
    WSGD_50 = np.empty((n_days,count,))*np.NAN 
    WSGD_25 = np.empty((n_days,count,))*np.NAN 
    WSGD_75 = np.empty((n_days,count,))*np.NAN 
    WSGD_obs = np.empty((n_days,count,))*np.NAN 
    
    #plot
    for i in range(count):
      # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
      WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
      WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
      WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
      if obs_flag_list[i] == 1:
        WSGD_obs[:, i] = WSGD_3D[:, -1, i]

    #================================== 
    #1) Plot WSGD Water stress
    nrow = math.ceil(count/2.0)
    ncol = 2
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Water Stress Index (WSGD)')
    s_count = 0  #scenario count
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    for k in range(nrow*ncol):
      # add every single subplot to the figure with a for loop
      ax = fig.add_subplot(nrow, ncol,Position[k])
      xdata = range(len(WSGD_50[:,s_count]))
      ax.plot(xdata, WSGD_50[:,s_count], 'b-', label = 'WGEN')
      ax.fill_between(xdata, WSGD_25[:, s_count], WSGD_75[:, s_count], alpha=0.2)

      ax.set_title(scename[s_count]+' (SCF)')
      ax.set_ylim([0, 1])
      if obs_flag_list[s_count] == 1:
        ax.plot(WSGD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
      if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
        ax.set(xlabel='Days After Planting [DAP]', ylabel='WSGD [-]')
      s_count = s_count + 1
      if s_count >= count:
        break
    # for ax in axs.flat:
    #   ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
    # # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #   ax.label_outer()
    # fig.set_size_inches(13, 8)
    # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
    # plt.savefig(fig_name,dpi=100)
    plt.show()

  def WSGD_plot_hist(self,fname, fname_hist, scename, obs_flag_list): #, cr_list)
    count = len(scename)
    n_days = 180 #approximate growing days
    nrealiz = 101 #100 realization + 1 observation case
    WSGD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              WSGD_3D[d_count, n_count, x] = float(line[238:243]) #WFGD   H20 factor,gr   Water factor for growth (0-1)  for WHEAT
            else:
              WSGD_3D[d_count, n_count, x] = float(line[127:133])  #WSGD   H20 stress,ex   Water stress - expansion/partioning/development (0-1)
            # WSGD_3D[d_count, n_count, x] = float(line[127:133])
            d_count = d_count + 1

    # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    WSGD_50 = np.empty((n_days,count,))*np.NAN 
    WSGD_25 = np.empty((n_days,count,))*np.NAN 
    WSGD_75 = np.empty((n_days,count,))*np.NAN 
    WSGD_obs = np.empty((n_days,count,))*np.NAN 
    for i in range(count):
      # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
      WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
      WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
      WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
      if obs_flag_list[i] == 1:
        WSGD_obs[:, i] = WSGD_3D[:, -1, i]
    #==========================================================
    #read water stress from historical weather observations
    #=======================================================
    nyears = 70  #arbitrary number
    WSGD_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname_hist[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              WSGD_3D_hist[d_count, n_count, x] = float(line[238:243]) #WFGD   H20 factor,gr   Water factor for growth (0-1)  for WHEAT
            else:
              WSGD_3D_hist[d_count, n_count, x] = float(line[127:133])  #WSGD   H20 stress,ex   Water stress - expansion/partioning/development (0-1)
            d_count = d_count + 1

    WSGD_50_hist = np.empty((n_days,count,))*np.NAN 
    WSGD_25_hist = np.empty((n_days,count,))*np.NAN 
    WSGD_75_hist = np.empty((n_days,count,))*np.NAN 

    for i in range(count):
      WSGD_50_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 50, axis = 1)
      WSGD_25_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 25, axis = 1)
      WSGD_75_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 75, axis = 1)
   
    #================================== 
    #1) Plot WSGD Water stress
    nrow = count
    ncol = 2  #first column for SCF and the second column for climatology 
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Water Stress Index [WSGD]')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      if k%2 == 0:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(WSGD_50[:,s_count]))
        ax.plot(xdata, WSGD_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, WSGD_25[:, s_count], WSGD_75[:, s_count], alpha=0.2)

        ax.set_title(scename[s_count]+' (SCF)')
        ax.set_ylim([0, 1])
        if obs_flag_list[s_count] == 1:
          ax.plot(WSGD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='WSGD [-]')
      else:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(WSGD_50_hist[:,s_count]))
        ax.plot(xdata, WSGD_50_hist[:,s_count], 'b-', label = 'Climatology')
        ax.fill_between(xdata, WSGD_25_hist[:, s_count], WSGD_75_hist[:, s_count], alpha=0.2)

        ax.set_title(scename[s_count]+' (Climatology)')
        ax.set_ylim([0, 1])
        if obs_flag_list[s_count] == 1:
          ax.plot(WSGD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]')
        s_count = s_count + 1
    plt.show()

################################################################################
  def NSTD_plot(self,fname, scename, obs_flag_list): #, cr_list):
    count = len(scename)
    n_days = 180 #approximate growing days
    nrealiz = 101 #100 realization + 1 observation case
    NSTD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weahter realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              NSTD_3D[d_count, n_count, x] = float(line[250:255])  #NFPD   N FACTOR,PHOTSN N factor for photosynthesis (0-1) for wheat
            else:
              NSTD_3D[d_count, n_count, x] = float(line[134:140]) #NSTD   N STRESS FACTOR Nitrogen stress factor (0-1) 
            # NSTD_3D[d_count, n_count, x] = float(line[134:140])
            d_count = d_count + 1

    # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN    
    NSTD_50 = np.empty((n_days,count,))*np.NAN 
    NSTD_25 = np.empty((n_days,count,))*np.NAN 
    NSTD_75 = np.empty((n_days,count,))*np.NAN 
    NSTD_obs = np.empty((n_days,count,))*np.NAN 
    
    #plot
    for i in range(count):
      # NSTD_avg[:, i] = np.nanmean(NSTD_3D[:, 0:100, i], axis = 1)
      NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
      NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
      NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
      if obs_flag_list[i] == 1:
        NSTD_obs[:, i] = NSTD_3D[:, -1, i]

    #================================== 
    #1) Plot NSTD Nitrogen stress
    nrow = math.ceil(count/2.0)
    ncol = 2
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Nitrogen Stress Index (NSTD)')
    s_count = 0  #scenario count
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    for k in range(nrow*ncol):
      # add every single subplot to the figure with a for loop
      ax = fig.add_subplot(nrow, ncol,Position[k])
      xdata = range(len(NSTD_50[:,s_count]))
      ax.plot(xdata, NSTD_50[:,s_count], 'b-', label = 'WGEN')
      ax.fill_between(xdata, NSTD_25[:, s_count], NSTD_75[:, s_count], alpha=0.2)

      ax.set_title(scename[s_count]+' (SCF)')
      ax.set_ylim([0, 1])
      if obs_flag_list[s_count] == 1:
        ax.plot(NSTD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
      if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
        ax.set(xlabel='Days After Planting [DAP]', ylabel='NSTD [-]')
      s_count = s_count + 1
      if s_count >= count:
        break
    
    # fig.set_size_inches(13, 8)
    # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
    # plt.savefig(fig_name,dpi=100)
    plt.show()

  def NSTD_plot_hist(self,fname, fname_hist, scename, obs_flag_list): #, cr_list)
    count = len(scename)
    n_days = 180 #approximate growing days
    nrealiz = 101 #100 realization + 1 observation case
    NSTD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              NSTD_3D[d_count, n_count, x] = float(line[250:255])  #NFPD   N FACTOR,PHOTSN N factor for photosynthesis (0-1) for wheat
            else:
              NSTD_3D[d_count, n_count, x] = float(line[134:140]) #NSTD   N STRESS FACTOR Nitrogen stress factor (0-1) 
            # NSTD_3D[d_count, n_count, x] = float(line[134:140])
            d_count = d_count + 1

    # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    NSTD_50 = np.empty((n_days,count,))*np.NAN 
    NSTD_25 = np.empty((n_days,count,))*np.NAN 
    NSTD_75 = np.empty((n_days,count,))*np.NAN 
    NSTD_obs = np.empty((n_days,count,))*np.NAN 
    for i in range(count):
      # NSTD_avg[:, i] = np.nanmean(NSTD_3D[:, 0:100, i], axis = 1)
      NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
      NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
      NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
      if obs_flag_list[i] == 1:
        NSTD_obs[:, i] = NSTD_3D[:, -1, i]
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    nyears = 70  #arbitrary number
    NSTD_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname_hist[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              NSTD_3D_hist[d_count, n_count, x] = float(line[250:255])  #NFPD   N FACTOR,PHOTSN N factor for photosynthesis (0-1) for wheat
            else:
              NSTD_3D_hist[d_count, n_count, x] = float(line[134:140]) #NSTD   N STRESS FACTOR Nitrogen stress factor (0-1) 
            # NSTD_3D_hist[d_count, n_count, x] = float(line[134:140])
            d_count = d_count + 1

    NSTD_50_hist = np.empty((n_days,count,))*np.NAN 
    NSTD_25_hist = np.empty((n_days,count,))*np.NAN 
    NSTD_75_hist = np.empty((n_days,count,))*np.NAN 

    for i in range(count):
      NSTD_50_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 50, axis = 1)
      NSTD_25_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 25, axis = 1)
      NSTD_75_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 75, axis = 1)
   
    #================================== 
    #1) Plot NSTD Nitrogen stress
    nrow = count
    ncol = 2  #first column for SCF and the second column for climatology
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Nitrogen Stress Index (NSTD)')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      if k%2 == 0:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(NSTD_50[:,s_count]))
        ax.plot(xdata, NSTD_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, NSTD_25[:, s_count], NSTD_75[:, s_count], alpha=0.2)

        ax.set_title(scename[s_count]+' (SCF)')
        ax.set_ylim([0, 1])
        if obs_flag_list[s_count] == 1:
          ax.plot(NSTD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='NSTD [-]')
      else:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(NSTD_50_hist[:,s_count]))
        ax.plot(xdata, NSTD_50_hist[:,s_count], 'b-', label = 'Climatology')
        ax.fill_between(xdata, NSTD_25_hist[:, s_count], NSTD_75_hist[:, s_count], alpha=0.2)

        ax.set_title(scename[s_count]+' (Climatology)')
        ax.set_ylim([0, 1])
        if obs_flag_list[s_count] == 1:
          ax.plot(NSTD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]')
        
        s_count = s_count + 1
    plt.show()
    # for ax in axs.flat:
    #     ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
    # # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #     ax.label_outer()
    # # fig.set_size_inches(13, 8)
    # # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
    # # plt.savefig(fig_name,dpi=100)
    plt.show()

 ##########################################################################
  def weather_plot_hist(self, fname, fname_hist, scename, obs_flag_list):
    count = len(scename)
    nrealiz = 101 #100 realization + 1 observation case
    n_days = 220 # #approximate growing days
    TMXD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    TMND_3D = np.empty((n_days,nrealiz,count,))*np.NAN
    SRAD_3D = np.empty((n_days,nrealiz,count,))*np.NAN
    # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
    # TMXD   MAX TEMP °C     Maximum daily temperature (°C)
    # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
    # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
    # TAVD   AVG TEMP °C     Average daily temperature (°C)
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      with open(fname[x]) as fp:
        for line in fp:
          if line[12:15] == '  1':   #line[12:15] => DAS
          # if line[6:9] == repr(plt_date[i]).zfill(3):   #line[6:9] => DOY
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            TMXD_3D[d_count, n_count, x] = float(line[60:64])
            TMND_3D[d_count, n_count, x] = float(line[67:71])
            SRAD_3D[d_count, n_count, x] = float(line[39:43])
            d_count = d_count + 1

    TMXD_50 = np.empty((n_days,count,))*np.NAN
    TMXD_25 = np.empty((n_days,count,))*np.NAN
    TMXD_75 = np.empty((n_days,count,))*np.NAN
    TMND_50 = np.empty((n_days,count,))*np.NAN
    TMND_25 = np.empty((n_days,count,))*np.NAN
    TMND_75 = np.empty((n_days,count,))*np.NAN
    SRAD_50 = np.empty((n_days,count,))*np.NAN
    SRAD_25 = np.empty((n_days,count,))*np.NAN
    SRAD_75 = np.empty((n_days,count,))*np.NAN
    TMXD_obs = np.empty((n_days,count,))*np.NAN 
    TMND_obs = np.empty((n_days,count,))*np.NAN 
    SRAD_obs = np.empty((n_days,count,))*np.NAN 

    #plot
    for i in range(count):
      if obs_flag_list[i] == 1:
        nyears = 101  #100 realization + 1 obs
        TMXD_obs[:, i] = TMXD_3D[:, -1, i]
        TMND_obs[:, i] = TMND_3D[:, -1, i]
        SRAD_obs[:, i] = SRAD_3D[:, -1, i]
      else:
        nyears = 100
      #count n-th day when all n-years values are available
      temp = np.sum(~np.isnan(TMXD_3D[:,:,i]), axis = 1)
      index = np.argwhere(temp < nyears)
      TMXD_50[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:nyears, i], 50, axis = 1)
      TMXD_25[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:nyears, i], 25, axis = 1)
      TMXD_75[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:nyears, i], 75, axis = 1)
      TMND_50[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:nyears, i], 50, axis = 1)
      TMND_25[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:nyears, i], 25, axis = 1)
      TMND_75[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:nyears, i], 75, axis = 1)
      SRAD_50[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:nyears, i], 50, axis = 1)
      SRAD_25[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:nyears, i], 25, axis = 1)
      SRAD_75[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:nyears, i], 75, axis = 1)
    #==================================
    #1) Plot all three variables (Tmax, Tmin, SRad) together
    nrow = count
    ncol = 3  #each column for Tmax, Tmin and SRAD repectively
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True) #number of subplots are hard-coded
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Temperature and Solar radiation')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      if k%3 == 0:  #first column for Tmax
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(TMXD_50[:,s_count]))
        ax.plot(xdata, TMXD_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, TMXD_25[:, s_count], TMXD_75[:, s_count], alpha=0.2)

        ax.set_title('Tmax - ' + scename[s_count]+' (SCF)')
        ax.set_ylim([15, 40])
        ax.grid(True)
        if obs_flag_list[s_count] == 1:
          ax.plot(TMXD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='Tempeature [\'C]')
      elif k%3 == 1:  #second column for Tmin
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(TMND_50[:,s_count]))
        ax.plot(xdata, TMND_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, TMND_25[:, s_count], TMND_75[:, s_count], alpha=0.2)

        ax.set_title('Tmin - ' + scename[s_count]+' (SCF)')
        ax.set_ylim([5, 25])
        ax.grid(True)
        if obs_flag_list[s_count] == 1:
          ax.plot(TMND_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='Tempeature [\'C]')
      else:    #thrid column for SRAD
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(SRAD_50[:,s_count]))
        ax.plot(xdata, SRAD_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, SRAD_25[:, s_count], SRAD_75[:, s_count], alpha=0.2)

        ax.set_title('SRAD - ' + scename[s_count]+' (SCF)')
        ax.set_ylim([10, 30])
        ax.grid(True)
        if obs_flag_list[s_count] == 1:
          ax.plot(SRAD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]') 

        s_count = s_count + 1      
    plt.show()
    # for j in range(nrow): #each row for each scenarios
    #   if s_count > (count-1):  #for a remaining empty plot
    #     xdata = range(len(TMXD_50[:,0]))
    #     axs[j, 0].plot(xdata, TMXD_50[:, s_count-1], 'w')
    #     xdata = range(len(TMND_50[:,0]))
    #     axs[j, 1].plot(xdata, TMND_50[:, s_count-1], 'w')
    #     xdata = range(len(SRAD_50[:,0]))
    #     axs[j, 2].plot(xdata, SRAD_50[:, s_count-1], 'w')
    #   else:
    #     #first column for TMAX
    #     xdata = range(len(TMXD_50[:,s_count]))
    #     yerr_low = TMXD_50[:, s_count] -TMXD_25[:, s_count]
    #     yerr_up = TMXD_75[:, s_count] - TMXD_50[:, s_count]
    #     axs[j, 0].errorbar(xdata, TMXD_50[:, s_count], yerr=yerr_low, uplims=True)
    #     axs[j, 0].errorbar(xdata, TMXD_50[:, s_count], yerr=yerr_up, lolims=True)
    #     if obs_flag_list[s_count] == 1:
    #       axs[j, 0].plot(TMXD_obs[:, s_count],'x-.', label='w/ obs. weather')
    #     axs[j, 0].set_title('Tmax' + scename[s_count])
    #     #second column for TMIN
    #     xdata = range(len(TMND_50[:,s_count]))
    #     yerr_low = TMND_50[:, s_count] -TMND_25[:, s_count]
    #     yerr_up = TMND_75[:, s_count] - TMND_50[:, s_count]
    #     axs[j, 1].errorbar(xdata, TMND_50[:, s_count], yerr=yerr_low, uplims=True)
    #     axs[j, 1].errorbar(xdata, TMND_50[:, s_count], yerr=yerr_up, lolims=True)
    #     if obs_flag_list[s_count] == 1:
    #       axs[j, 1].plot(TMND_obs[:, s_count],'x-.', label='w/ obs. weather')
    #     axs[j, 1].set_title('Tmin' + scename[s_count])
    #     #third column for SRAD
    #     xdata = range(len(SRAD_50[:,s_count]))
    #     yerr_low = SRAD_50[:, s_count] -SRAD_25[:, s_count]
    #     yerr_up = SRAD_75[:, s_count] - SRAD_50[:, s_count]
    #     axs[j, 2].errorbar(xdata, SRAD_50[:, s_count], yerr=yerr_low, uplims=True)
    #     axs[j, 2].errorbar(xdata, SRAD_50[:, s_count], yerr=yerr_up, lolims=True)
    #     if obs_flag_list[s_count] == 1:
    #       axs[j, 2].plot(SRAD_obs[:, s_count],'x-.', label='w/ obs. weather')
    #     axs[j, 2].set_title('Tmin' + scename[s_count])
    #   s_count = s_count + 1

    # for ax in axs.flat:
    #   ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C] or SRAD[MJ/(m2.d)]')
    # # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #   ax.label_outer()
    # # fig.set_size_inches(12, 8)
    # # fig_name = Wdir_path + "\\"+ scename[0] +"_output\\Tmax_time_series.png"
    # # plt.savefig(fig_name,dpi=100)
    # plt.show()

##############################################################
  def cum_rain_plot_hist(self, fname, fname_hist, scename, obs_flag_list):
    count = len(scename)
    nrealiz = 101 #100 realization + 1 observation case
    n_days = 220 # #approximate growing days
    PREC_3D= np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      with open(fname[x]) as fp:
        for line in fp:
          if line[12:15] == '  1':   #line[12:15] => DAS
          # if line[6:9] == repr(plt_date[i]).zfill(3):   #line[6:9] => DOY
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            PREC_3D[d_count, n_count, x] = float(line[16:22])
            d_count = d_count + 1

    PREC_50 = np.empty((n_days,count,))*np.NAN
    PREC_25 = np.empty((n_days,count,))*np.NAN
    PREC_75 = np.empty((n_days,count,))*np.NAN
    PREC_obs = np.empty((n_days,count,))*np.NAN 
    max_index = 1
    for i in range(count):
      if obs_flag_list[i] == 1:
        nyears = 101  #100 realization + 1 obs
        PREC_obs[:, i] = PREC_3D[:, -1, i]
      else:
        nyears = 100
      #count n-th day when all n-years values are available
      temp = np.sum(~np.isnan(PREC_3D[:,:,i]), axis = 1)
      index = np.argwhere(temp < nyears)
      PREC_50[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D[0:index[0][0], 0:nyears, i], axis=0), 50, axis = 1)
      PREC_25[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D[0:index[0][0], 0:nyears, i], axis=0), 25, axis = 1)
      PREC_75[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D[0:index[0][0], 0:nyears, i], axis=0), 75, axis = 1)
      if index[0][0] > max_index:
        max_index = index[0][0]
    #trim the array before visualization
    PREC_50 = PREC_50[0:max_index,:]
    PREC_25 = PREC_25[0:max_index,:]
    PREC_75 = PREC_75[0:max_index,:]
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    nyears = 70  #arbitrary number
    PREC_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[12:15] == '  1':   #line[12:15] => DAS
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            PREC_3D_hist[d_count, n_count, x] = float(line[16:22])
            d_count = d_count + 1

    PREC_50_hist = np.empty((n_days,count,))*np.NAN
    PREC_25_hist = np.empty((n_days,count,))*np.NAN
    PREC_75_hist = np.empty((n_days,count,))*np.NAN 
    max_index = 1
    for i in range(count):
      #count n-th day when all n-years values are available
      temp = np.sum(~np.isnan(PREC_3D_hist[:,:,i]), axis = 1)
      nyear = temp[0]
      index = np.argwhere(temp < nyear)
      if index[0][0] < 60:  #EJ(6/4/2020) 80 days are arbitrary to avoid cases of crop failur in the beginning
        index[0][0] = 60
      # np.cumsum(a,axis=0)
      PREC_50_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 50, axis = 1)
      PREC_25_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 25, axis = 1)
      PREC_75_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 75, axis = 1)
      if index[0][0] > max_index:
        max_index = index[0][0]
    #trim the array before visualization
    PREC_50_hist = PREC_50_hist[0:max_index,:]
    PREC_25_hist = PREC_25_hist[0:max_index,:]
    PREC_75_hist = PREC_75_hist[0:max_index,:]
    #==================================
    #1) Plot 
    nrow = count
    ncol = 2  #first column for SCF and the second column for climatology
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True) #number of subplots are hard-coded
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Cumulative Precipitation')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      if k%2 == 0:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(PREC_50[:,s_count]))
        ax.plot(xdata, PREC_50[:,s_count], 'b-', label = 'WGEN')
        ax.fill_between(xdata, PREC_25[:, s_count], PREC_75[:, s_count], alpha=0.2)
        # yerr_low = PREC_50[:, s_count] - PREC_25[:, s_count]
        # yerr_up = PREC_75[:, s_count] - PREC_50[:, s_count]
        # ax.errorbar(xdata, PREC_50[:,s_count], yerr=yerr_low, uplims=True, color='b')
        # ax.errorbar(xdata, PREC_50[:,s_count], yerr=yerr_up, lolims=True, color='g')
        ax.grid(True)
        ax.set_title(scename[s_count]+' (SCF)')
        if obs_flag_list[s_count] == 1:
          ax.plot(np.cumsum(PREC_obs[:, s_count], axis=0),'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='Precipitation [mm]')
      else:
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(PREC_50_hist[:,s_count]))
        ax.plot(xdata, PREC_50_hist[:,s_count], 'b-', label = 'climatology')
        ax.fill_between(xdata, PREC_25_hist[:, s_count], PREC_75_hist[:, s_count], alpha=0.2)

        ax.grid(True)
        ax.set_title(scename[s_count]+' (Climatology)')
        if obs_flag_list[s_count] == 1:
          ax.plot(np.cumsum(PREC_obs[:, s_count], axis=0),'x-.', label='w/ obs. weather', color='r')
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]')
        
        s_count = s_count + 1
    plt.show()

    # for ax in axs.flat:
    #   ax.set(xlabel='Days After Planting [DAP]', ylabel='Cumulative precipitation [mm]')
    # # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #   ax.label_outer()
    # fig.set_size_inches(12, 8)
    # fig_name = Wdir_path + "\\"+ scename[0] +"_output\\Tmax_time_series.png"
    # plt.savefig(fig_name,dpi=100)
    plt.show()
#=================================================
# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['fliers'][1], color='blue')
    plt.setp(bp['medians'][0], color='blue')

    plt.setp(bp['boxes'][1], color='red')
    plt.setp(bp['caps'][2], color='red')
    plt.setp(bp['caps'][3], color='red')
    plt.setp(bp['whiskers'][2], color='red')
    plt.setp(bp['whiskers'][3], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['medians'][1], color='red')

