#Author: Eunjin Han
#Institute: IRI-Columbia University
#Date: 2/21/2020
#Purpose: get indices of sorted years based on a certain period
#         e.g, one month or 3 months for any given SCF

import numpy as np
import pandas as pd
import calendar
import time



def get_ind_sorted_yr(wdir, WTD_df, sdoy, edoy): 
    #sdoy: starting doy of the target period
    #edoy: ending doy of the target period
    #===================================================
    sdoy = int(sdoy) #convert into integer just in case
    edoy = int(edoy)
    #3-1) Extract daily weather data for the target period
    # count how many years are recorded
    year_array = WTD_df.YEAR.unique()
    nyears = year_array.shape[0]

    #Make 2D array and aggregate during the specified season/months (10/15/2020)
    rain_array = np.reshape(WTD_df.RAIN.values, (nyears,365))
    if edoy > sdoy: #all months within the target season is within one year
        season_rain_sum = np.sum(rain_array[:,(sdoy-1):edoy], axis=1)
    else: #seasonal sum goes beyond the first year  #   if edoy < sdoy:
        a= rain_array[:-1,(sdoy-1):]
        b = rain_array[1:,0:edoy]
        rain_array2 = np.concatenate((a,b), axis = 1)
        # season_rain_sum = np.sum(rain_array[:-1,(sdoy-1):(sdoy+edoy)], axis=1)    
        season_rain_sum = np.sum(rain_array2, axis=1) #check !
        nyears = nyears - 1
    #================================================================
    #save dataframe into a csv file [Note: Feb 29th was excluded]
    df_sorted = pd.DataFrame(np.zeros((nyears, 3)))   
    df_sorted.columns = ['YEAR','season_rain', 'sindex']  #iyear => ith year
    df_sorted.name = 'season_rain_sorted'+str(sdoy)
    df_sorted.YEAR.iloc[:]= year_array[0:nyears][np.argsort(season_rain_sum)]
    df_sorted.season_rain.iloc[:]= season_rain_sum[np.argsort(season_rain_sum)]
    df_sorted.sindex.iloc[:]= np.argsort(season_rain_sum)
    #write dataframe into CSV file
    df_sorted.to_csv(wdir + '//'+df_sorted.name + '.csv', index=False)

    # return np.argsort(season_rain_sum)  # index_sort
    return df_sorted
#====================================================================

