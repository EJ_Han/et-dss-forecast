# -*- coding: utf-8 -*-

from SIMAGRI._compact import *
from SIMAGRI.process.errormessages import ModuleErrorMessage
import SIMAGRI.process.simagri_processes as simagri_procs


class SIMAGRIPROC:
    _UIParent = None
    _Setting = None

    # _Param__File_Prefix = "param_"

    _ModErrMsg = None

    def __init__(self, setting, uiparanet):
        self._Setting = setting
        self._UIParent = uiparanet

        self._ModErrMsg = ModuleErrorMessage(uiparanet)

    # # def RunCAMDT(self, ScenarioNo):
    # def RunDSSAT(self, ScenarioNo):

    #   if ScenarioNo == 1:
    #       # first scenario
    #       self.procDSSAT_CO(self._Setting.ScenariosSetup.Working_directory,
    #                         self._Setting.ScenariosSetup.WIfS_name1.getvalue()) #,
    #                         # self._Setting.TemporalDownscaling.WStation.getvalue())
    #   elif ScenarioNo == 2:
    #       # second scenario
    #       self.procDSSAT_CO(self._Setting.ScenariosSetup.Working_directory,
    #                         self._Setting.ScenariosSetup.WIfS_name2.getvalue()) #,
    #                         # self._Setting.TemporalDownscaling.WStation.getvalue())
    #   elif ScenarioNo == 3:
    #       # 3rd scenario
    #       self.procDSSAT_CO(self._Setting.ScenariosSetup.Working_directory,
    #                         self._Setting.ScenariosSetup.WIfS_name3.getvalue()) #,
    #                         # self._Setting.TemporalDownscaling.WStation.getvalue())
    #   elif ScenarioNo == 4:
    #       # 4th scenario
    #       self.procDSSAT_CO(self._Setting.ScenariosSetup.Working_directory,
    #                         self._Setting.ScenariosSetup.WIfS_name4.getvalue()) #,
    #                         # self._Setting.TemporalDownscaling.WStation.getvalue())
    #   else: #elif ScenarioNo == 4:
    #       # 5thscenario
    #       self.procDSSAT_CO(self._Setting.ScenariosSetup.Working_directory,
    #                         self._Setting.ScenariosSetup.WIfS_name5.getvalue()) #,
    #                         # self._Setting.TemporalDownscaling.WStation.getvalue())

    # def procDSSAT_CO(self, Working_directory, WIfS_name):
    #   entries = ("PlantGro.OUT", "Evaluate.OUT",
    #              "ET.OUT", "OVERVIEW.OUT", "PlantN.OUT", "SoilNi.OUT", "INFO.OUT", "Mulch.OUT", "RunList.OUT",
    #              "SoilNiBal.OUT", "SoilNoBal.OUT", "SoilTemp.OUT", "SoilWat.OUT", "SoilWatBal.OUT",
    #              "SolNBalSum.OUT", "Summary.OUT", "Weather.OUT")  # ,"WARNING.OUT"

    #   if self._Setting.DSSATSetup1.Crop_type.get() == 0:  #drybean
    #     param_SNX = "SEML" + WIfS_name + ".SNX"  # name1
    #     #write batch file before running DSSAT executable
    #     self.writeV47_main(Working_directory, param_SNX,'ML')
    #   elif self._Setting.DSSATSetup1.Crop_type.get() == 1:  #maize
    #     param_SNX = "SEPN" + WIfS_name + ".SNX"  # name1
    #     #write batch file before running DSSAT executable
    #     self.writeV47_main(Working_directory, param_SNX,'PN')

    #   if os.path.isfile(param_SNX):
    #     # # Delete *.WTD from previous runs
    #     # WTD_names = WStation[0][0:4] + "0*.WTD"
    #     # for file in os.listdir('.'):
    #     #   if fnmatch.fnmatch(file, WTD_names):
    #     #     try:
    #     #       os.remove(file)
    #     #     except Exception as e:
    #     #       traceback.print_exc()

    #     # args = "%s %s" %(simagri_procs.CAMDT_Model, param_SNX)  # "CAMDT_PH_exe " + param_SNX
    #     #==RUN DSSAT with ARGUMENT
    #     if self._Setting.DSSATSetup1.Crop_type.get() == 1:  #maize
    #       args = "%s %s" %(simagri_procs.DSSAT_Model, 'N DSSBatch.v47')  # args = "DSCSM046.EXE PNCER046 B DSSBatch.v46"
    #     elif self._Setting.DSSATSetup1.Crop_type.get() == 0:  #drybean
    #       args = "%s %s" %(simagri_procs.DSSAT_Model, 'N DSSBatch.v47')  # args = "DSCSM046.EXE CRGRO046 B DSSBatch.v46"

    #     # ===Run executable with argument
    #     subprocess.call(args)  # , stdout=FNULL, stderr=FNULL, shell=False)
    #     # create a new folder to save outputs of the target scenario
    #     new_folder = WIfS_name + "_output"

    #     if os.path.exists(new_folder):
    #       shutil.rmtree(new_folder)  # remove existing folder
    #     os.makedirs(new_folder)

    #     # copy outputs to the new folder
    #     dest_dir = path.join(Working_directory, new_folder)
    #     for entry in entries:
    #       if os.path.isfile(entry):
    #         shutil.move(entry, dest_dir)

    #     # move SNX file to output subfolder (EJ: 2/13/2017)
    #     if (self._Setting.DSSATSetup1.Crop_type.get() == 0):  #drybean
    #       SNX_file = "SEML" + WIfS_name + ".SNX"  # name1
    #     elif (self._Setting.DSSATSetup1.Crop_type.get() == 1):  #maize
    #       SNX_file = "SEPN" + WIfS_name + ".SNX"  # name1
    #     if os.path.isfile(SNX_file):
    #         shutil.move(SNX_file, dest_dir)
    #     if os.path.isfile(param_SNX):
    #         shutil.move(param_SNX, dest_dir)

    def writeSNX(self, SNX_fname, cr_type, obs_flag):
        # =============================fertilizer input error check
        if self._Setting.DSSATSetup2.FA_day1.getvalue() != '':  #days after planting
            if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] == 'None' or self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0] == 'None': 
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_day2.getvalue() != '':  #days after planting
            if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] == 'None' or self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0] == 'None': 
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_day3.getvalue() != '':  #days after planting
            if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] == 'None' or self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0] == 'None': 
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_day4.getvalue() != '':  #days after planting
            if self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0] == 'None' or self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0] == 'None': 
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_day5.getvalue() != '':  #days after planting
            if self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0] == 'None' or self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0] == 'None': 
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != 'None' or self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0]  != 'None': 
            if self._Setting.DSSATSetup2.FA_day1.getvalue() == '':  #if there is no fert application dates
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != 'None' or self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0]  != 'None': 
            if self._Setting.DSSATSetup2.FA_day2.getvalue() == '':  #if there is no fert application dates
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != 'None' or self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0]  != 'None': 
            if self._Setting.DSSATSetup2.FA_day3.getvalue() == '':  #if there is no fert application dates
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0] != 'None' or self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0]  != 'None': 
            if self._Setting.DSSATSetup2.FA_day4.getvalue() == '':  #if there is no fert application dates
                self._ModErrMsg.fertilizer_err.activate()
        if self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0] != 'None' or self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0]  != 'None': 
            if self._Setting.DSSATSetup2.FA_day5.getvalue() == '':  #if there is no fert application dates
                self._ModErrMsg.fertilizer_err.activate()

        # Error message if SCF info is missing (EJ:3/6/2020)
        if self._Setting.DSSATSetup1.trimester1.getvalue()[0] == "":  # check
            self._ModErrMsg.trimester_err.activate()
        if self._Setting.DSSATSetup1.BN1.getvalue() == "" or self._Setting.DSSATSetup1.AN1.getvalue() == "":
            self._ModErrMsg.SCF_err.activate()
        if self._Setting.DSSATSetup1.BN2.getvalue() == "" or self._Setting.DSSATSetup1.AN2.getvalue() == "":
            self._ModErrMsg.SCF_err.activate()

        # ========= wirte SNX
        self.writeSNX_main(SNX_fname, cr_type, obs_flag)
        return

    # =====================================================================
    def writeSNX_main(self, SNX_fname, crop, obs_flag):
        if crop == 'WH':
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETWHTEMP.SNX")
        elif crop == 'MZ':
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETMZTEMP.SNX")
        else:  # SG
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETSGTEMP.SNX")
        fr = open(temp_snx, "r")  # opens temp SNX file to read
        fw = open(SNX_fname, "w")  # opens SNX file to write

        # ===set up parameters
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
            MI = '1'  # should be one character
        else:
            MI = '0' 
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
            MF = '1'
        else:  # no fertilizer
            MF = '0'

        if crop == 'WH':  # Wheat
            INGENO = self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0][0:6]  # "CI0001 P30F35"
            CNAME = self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0][7:]  # "CI0001 P30F35"
            ID_SOIL = self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.WH_plt_density.getvalue()  #planting density
            i_NO3=self._Setting.DSSATSetup1.MZ_ini_NO3.getvalue()[0][0:1]
            # i_NO3 = 'L'  # by default, low initial NO3
            IC_w_ratio = float(self._Setting.DSSATSetup1.WH_ini_H2O.getvalue()[0][0:3])
        elif crop == 'MZ':  # Maize
            INGENO = self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0][0:6]  
            CNAME = self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0][7:]  
            ID_SOIL = self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.MZ_plt_density.getvalue()  #planting density
            i_NO3 = self._Setting.DSSATSetup1.MZ_ini_NO3.getvalue()[0][0:1]  # self.label_04.cget("text")[0:1]  #self.NO3_soil.getvalue()[0][0:1] #'H' #or 'L'
            IC_w_ratio = float(self._Setting.DSSATSetup1.MZ_ini_H2O.getvalue()[0][0:3])
        else: #sorghum
            INGENO = self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][0:6]  
            CNAME = self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][7:]  
            ID_SOIL = self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.SG_plt_density.getvalue()  #planting density
            i_NO3 = self._Setting.DSSATSetup1.SG_ini_NO3.getvalue()[0][0:1]  # self.label_04.cget("text")[0:1]  #self.NO3_soil.getvalue()[0][0:1] #'H' #or 'L'
            IC_w_ratio = float(self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0][0:3])

        WSTA = self._Setting.DSSATSetup1.WStation.getvalue()[0][0:4]  
        # convert year-month-date to DOY
        plt_year = self._Setting.DSSATSetup1.plt_year.getvalue()
        temp = plt_year + '-' + self._Setting.DSSATSetup1.plt_month.getvalue() + '-' + self._Setting.DSSATSetup1.plt_date.getvalue()
        plt_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
        if (plt_doy - 1) == 0:  # initial condition: 1 day before planting
            temp_year = int(plt_year) - 1
            if calendar.isleap(temp_year):
                ICDAT = repr(temp_year)[2:] + '366'
            else:
                ICDAT = repr(temp_year)[2:] + '365'
        else:
            ICDAT = plt_year[2:] + repr(plt_doy - 1).zfill(3)

        SNH4 = 1.5  # **EJ(5/27/2015) followed by Walter's UYPNDSS6.SNX
        PDATE = plt_year[2:] + repr(plt_doy).zfill(3)
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied       
            #EJ(10/20) determine fertilizer info 
            fert_count = 0
            FDATE = []
            FMCD = []
            FACD = []
            FDEP = []
            FAMN = []
            FAMP = []
            FAMK = []
            if self._Setting.DSSATSetup2.FA_day1.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth1.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day1.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0][0:5])  # application method     
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth1.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N1.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N1.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P1.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P1.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K1.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K1.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day2.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth2.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day2.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth2.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N2.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N2.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P2.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P2.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K2.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K2.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day3.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth3.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day3.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0][0:5])  # application method     
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth3.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N3.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N3.getvalue())
                            FAMP.append('-99') #if #self._Setting.DSSATSetup2.FA_amt_P3.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P3.getvalue())
                            FAMK.append('-99') #if #self._Setting.DSSATSetup2.FA_amt_K3.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K3.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day4.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth4.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day4.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth4.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N4.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N4.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P4.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P4.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K4.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K4.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day5.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth5.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day5.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth5.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N5.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N5.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P5.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P5.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K5.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K5.getvalue())
                            fert_count = fert_count +1
        SDATE = ICDAT  # EJ(3/5/2020) #simuation starting date
        NYERS = "1"  # repr(int(end_year)-int(start_year)+1) #'12'
        # if int(plt_date) > 180: #when planted after July 1, remove the last year
        #   NYERS = NYERS-1
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
            IRRIG = 'R'
            IMETH = self._Setting.DSSATSetup2.IR_method.getvalue()[0][0:5]  # 'IR001' #irrigation method
        elif self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  # automatic when required
            IRRIG = 'A'  # automatic, or 'N' (no irrigation)
            IMDEP = self._Setting.DSSATSetup2.IA_mng_depth.getvalue()
            ITHRL = self._Setting.DSSATSetup2.IA_threshold.getvalue()
            IREFF = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue()
        else:
            IRRIG = 'N'  # automatic, or 'N' (no irrigation)
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied
            FERTI = 'D'  # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
        else:
            FERTI = 'N'
        # ===end of setting up paramters

        # read lines 1-9 from temp file
        for line in range(0, 14):
            temp_str = fr.readline()
            fw.write(temp_str)

        # write *TREATMENTS
        # CU='1'
        # #EJ(10/22/2020) Addd Soil Analysis section if P is simulated
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':
        #     SA = '1' 
        # else:
        #     SA = '0'
        SA = '0'
        IC = '1'
        MP = '1'
        MR = '0'
        MC = '0'
        MT = '0'
        ME = '0'
        MH = '0'
        SM = '1'
        temp_str = fr.readline()
        for i in range(0, 100):
            FL = str(i + 1)
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(
                FL.rjust(3), '1 0 0 ERiMA DCC1                 1',
                FL.rjust(3), SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3),
                MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))
            fw.write(" \n")
        if obs_flag == 1:
            FL = str(101)  # if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(
                FL.rjust(3), '1 0 0 ERiMA DCC1                 1',
                FL.rjust(3), SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3),
                MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))
            fw.write(" \n")

            # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)

        # write *CULTIVARS
        temp_str = fr.readline()
        new_str = temp_str[0:3] + crop + temp_str[5:6] + INGENO + temp_str[12:13] + CNAME
        fw.write(new_str)
        fw.write(" \n")

        # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)

        # ================write *FIELDS
        # Get soil info from *.SOL
        SOL_file = path.join(self._Setting.ScenariosSetup.Working_directory, "ET.SOL")
        # soil_depth, wp, fc, nlayer = get_soil_IC(SOL_file, ID_SOIL)
        soil_depth, wp, fc, nlayer, SLTX = get_soil_IC(SOL_file, ID_SOIL)
        SLDP = repr(soil_depth[-1])
        for i in range(0, 100):
            FL = str(i + 1)
            ID_FIELD = WSTA + str(i + 1).zfill(4)  # WSTA = 'SANJ'
            WSTA_ID = str(i + 1).zfill(4)
            fw.write(
                '{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5),
                                                                             '       -99   -99   -99   -99   -99   -99 ',
                                                                             SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,
                                                                             ' -99'))
            fw.write(" \n")
        if obs_flag == 1:
            FL = str(101)  # if observed weather is available, run #101 treatment with observed weather
            ID_FIELD = WSTA + str(101).zfill(4)  # WSTA = 'SANJ'
            WSTA_ID = WSTA
            fw.write(
                '{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5),
                                                                             '       -99   -99   -99   -99   -99   -99 ',
                                                                             SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,
                                                                             ' -99'))
            fw.write(" \n")

        temp_str = fr.readline()  # 1 -99      CCER       -99   -99 DR000   -99   -99
        temp_str = fr.readline()  # @L ...........XCRD ...........YCRD .....ELEV
        fw.write(temp_str)
        temp_str = fr.readline()  # 1             -99             -99       -99   ==> skip
        # ================write *FIELDS - second section
        for i in range(0, 100):
            FL = str(i + 1)
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3),
                                            '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")
        if obs_flag == 1:
            FL = str(101)  # if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3),
                                            '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")
        fw.write(" \n")

        # #EJ(10/22/2020) Addd Soil Analysis section if P is simulated
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':
        #     fw.write('*SOIL ANALYSIS'+ "\n")
        #     fw.write('@A SADAT  SMHB  SMPX  SMKE  SANAME'+ "\n")
        #     fw.write(' 1 '+ ICDAT + ' SA011 SA001 SA014  -99'+ "\n")
        #     fw.write('@A  SABL  SADM  SAOC  SANI SAPHW SAPHB  SAPX  SAKE  SASC'+ "\n")
        #     soil_depth, SADM, SAOC, SANI, SAPHW = get_soil_SA(SOL_file, ID_SOIL)
        #     if self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'V':  #very low
        #         SAPX = '   2.0'
        #     elif self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'L':  #Low
        #         SAPX = '   7.0'
        #     elif self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'M':  #Medium
        #         SAPX = '  12.0'
        #     else:   #high
        #         SAPX = '  18.0'
        #     for i in range(0, len(soil_depth)):
        #         new_str = ' 1'+ repr(soil_depth[i]).rjust(6) + repr(SADM[i]).rjust(6) + repr(SAOC[i]).rjust(6) + repr(SANI[i]).rjust(6) + repr(SAPHW[i]).rjust(6)+ '   -99' + SAPX + '   -99   -99'+"\n"
        #         fw.write(new_str)


        # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)
        # write *INITIAL CONDITIONS
        temp_str = fr.readline()
        new_str = temp_str[0:9] + ICDAT + temp_str[14:]
        fw.write(new_str)
        temp_str = fr.readline()  # @C  ICBL  SH2O  SNH4  SNO3
        fw.write(temp_str)

        # #Get soil info from *.SOL
        # SOL_file=path.join(self._Setting.ScenariosSetup.Working_directory, "CC.SOL")
        # soil_depth, wp, fc, nlayer = get_soil_IC(SOL_file, ID_SOIL)
        temp_str = fr.readline()
        for nline in range(0, nlayer):
            if nline == 0:  # first layer
                temp_SH2O = IC_w_ratio * (fc[nline] - wp[nline]) + wp[nline]  # EJ(6/25/2015): initial AWC=70% of maximum AWC
                # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3 = '15'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga1':
                    #     SNO3 = '14'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCTolima01' or ID_SOIL == 'CCEspi2014':
                    #     SNO3 = '13'  # **EJ(3/2/2017
                    # else:
                    #     SNO3 = '15'  # **EJ(3/2/2017
                else:  # i_NO3 == 'L':
                    SNO3 = '5'  # **EJ(5/27/2015)
            elif nline == 1:  # second layer
                temp_SH2O = IC_w_ratio * (fc[nline] - wp[nline]) + wp[nline]  # EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3 = '2'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga0':
                    #     SNO3 = '0.5'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCCienaga2':
                    #     SNO3 = '1.4'
                    # elif ID_SOIL == 'CCBuga2014':
                    #     SNO3 = '2.7'
                    # else:
                    #     SNO3 = '0.0'
                        ##self.soiltype_err.activate()
                else:  # elif i_NO3 == 'L':
                    SNO3 = '.5'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga0':
                    #     SNO3 = '0.3'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCCienaga2':
                    #     SNO3 = '0.6'
                    # elif ID_SOIL == 'CCBuga2014':
                    #     SNO3 = '1.3'
                    # else:
                    #     SNO3 = '0.0'
            else:
                temp_SH2O = fc[nline]  # float
                SNO3 = '0'  # **EJ(5/27/2015)
            SH2O = repr(temp_SH2O)[0:5]  # convert float to string
            new_str = temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22] + SNO3.rjust(4) + "\n"
            fw.write(new_str)
        fw.write("  \n")

        for nline in range(0, 10):
            temp_str = fr.readline()
            # print temp_str
            if temp_str[0:9] == '*PLANTING':
                break

        fw.write(temp_str)  # *PLANTING DETAILS
        temp_str = fr.readline()  # @P PDATE EDATE
        fw.write(temp_str)

        # write *PLANTING DETAILS
        temp_str = fr.readline()
        PPOE = PPOP #planting density 
        new_str = temp_str[0:3] + PDATE + '   -99' + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
        fw.write(new_str)
        fw.write("  \n")

        # write *IRRIGATION AND WATER MANAGEMENT, if irrigation on reported dates
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  
            fw.write('*IRRIGATION AND WATER MANAGEMENT'+ "\n")
            fw.write('@I  EFIR  IDEP  ITHR  IEPT  IOFF  IAME  IAMT IRNAME'+ "\n")
            fw.write(' 1     1    30    50   100 GS000 IR001    10 -99'+ "\n")
            fw.write('@I IDATE  IROP IRVAL'+ "\n")
            IROP = self._Setting.DSSATSetup2.IR_method.getvalue()[0][0:5]  #irrigation method
            #irrgation appliation 1
            if self._Setting.DSSATSetup2.ir_m1.getvalue() != '' and self._Setting.DSSATSetup2.ir_d1.getvalue() != '' and self._Setting.DSSATSetup2.ir_y1.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt1.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y1.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m1.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d1.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt1.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 2
            if self._Setting.DSSATSetup2.ir_m2.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y2.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt2.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y2.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m2.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d2.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt2.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m3.getvalue() != '' and self._Setting.DSSATSetup2.ir_d3.getvalue() != '' and self._Setting.DSSATSetup2.ir_y3.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt3.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y3.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m3.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d3.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt3.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m4.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y4.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt4.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y4.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m4.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d4.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt4.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m5.getvalue() != '' and self._Setting.DSSATSetup2.ir_d5.getvalue() != '' and self._Setting.DSSATSetup2.ir_y5.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt5.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y5.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m5.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d5.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt5.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #end of writing irrigation application

        # write *FERTILIZERS (INORGANIC)
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied
            if fert_count > 0:   # fertilizer applied
                # read lines from temp file
                for line in range(0, 3):
                    temp_str = fr.readline()
                    fw.write(temp_str)
                temp_str = fr.readline()
                for i in range(fert_count):
                    new_str = temp_str[0:5] + FDATE[i].rjust(3) + ' ' + FMCD[i].rjust(5) + ' ' + FACD[i].rjust(5) + ' ' + FDEP[i].rjust(5) + ' ' + FAMN[i].rjust(5) + ' ' + FAMP[i].rjust(5) + ' ' + FAMK[i].rjust(5) + temp_str[44:]
                    fw.write(new_str)
                    # fw.write(" \n")
                temp_str = fr.readline()

        fw.write("  \n")
        for nline in range(0, 10):
            temp_str = fr.readline()
            # print temp_str
            if temp_str[0:11] == '*SIMULATION':
                break
        fw.write(temp_str)  # *SIMULATION CONTROLS
        temp_str = fr.readline()
        fw.write(temp_str)  # @N GENERAL     NYERS NREPS START SDATE RSEED SNAME
        # write *SIMULATION CONTROLS
        temp_str = fr.readline()
        new_str = temp_str[0:18] + NYERS.rjust(2) + temp_str[20:33] + SDATE + temp_str[38:]
        fw.write(new_str)
        temp_str = fr.readline()  # @N OPTIONS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 OP
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':  #if phosphorous simulation is "on"
        #     fw.write(' 1 OP              Y     Y     Y     Y     N     N     N     N     D'+ "\n")
        # else:
        #     fw.write(' 1 OP              Y     Y     Y     N     N     N     N     N     D'+ "\n")
        fw.write(' 1 OP              Y     Y     Y     N     N     N     N     N     D'+ "\n")
        temp_str = fr.readline()  # @N METHODS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 ME
        fw.write(temp_str)
        temp_str = fr.readline()  # @N MANAGEMENT
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 MA
        new_str = temp_str[0:25] + IRRIG + temp_str[26:31] + FERTI + temp_str[32:]
        fw.write(new_str)
        temp_str = fr.readline()  # @N OUTPUTS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 OU
        fw.write(temp_str)

        # read lines from temp file
        for line in range(0, 5):
            temp_str = fr.readline()
            fw.write(temp_str)
        # irrigation method
        temp_str = fr.readline()  # 1 IR
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  # automatic when required
            IMDEP = self._Setting.DSSATSetup2.IA_mng_depth.getvalue()
            ITHRL = self._Setting.DSSATSetup2.IA_threshold.getvalue()
            IREFF = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue()
            fw.write(' 1 IR          ' + IMDEP.rjust(5) + ITHRL.rjust(6) + '   100 GS000 IR001    10'+ IREFF.rjust(6)+ "\n")
        else:
            # new_str = temp_str[0:39] + IMETH + temp_str[44:]
            fw.write(temp_str)

        # read lines from temp file
        for line in range(0, 7):
            temp_str = fr.readline()
            fw.write(temp_str)

        fr.close()
        fw.close()

    # ====End of WRITE *.SNX

    def writeV47_main(self, sname, crop, obs_flag):  # sname includes full path
        sname = sname.replace("/", "\\")
        if crop == 'WH':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_WH.V47")
        elif crop == 'MZ':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_MZ.V47")
        else:  ##'SG':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_SG.V47")

        dv7_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch.V47")
        fr = open(temp_dv7, "r")  # opens temp DV4 file to read
        fw = open(dv7_fname, "w")
        # read template and write lines
        for line in range(0, 10):
            temp_str = fr.readline()
            fw.write(temp_str)

        temp_str = fr.readline()
        # new_str2='{:<95}'.format(sname)+ temp_str[95:]
        for j in range(100):
            # new_str2 = '{0:<95}{1:4s}'.format(sname, repr(j + 1).rjust(4)) + '      1      0      0      0'
            new_str2 = '{0:<95}{1:4s}'.format(sname, repr(j + 1).rjust(4)) + temp_str[99:]
            fw.write(new_str2)
        if obs_flag == 1:
            new_str2 = '{0:<95}{1:4s}'.format(sname, repr(101).rjust(4)) + temp_str[99:]
            fw.write(new_str2)
        fr.close()
        fw.close()

    # =============================================
    # Write scenario summary into a text file
    # =============================================
    def write_scen_summary(self, sname):
        # LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
        summary_fname = path.join(self._Setting.ScenariosSetup.Working_directory, sname + "_summary.txt")
        f = open(summary_fname, "w")
        f.write("Directory:  " + self._Setting.ScenariosSetup.Working_directory + "\n")
        f.write("Station_name: " + self._Setting.DSSATSetup1.WStation.getvalue()[0][0:4] + " \n")
        # f.write("LAT: " + repr(LAT) + " \n")
        # f.write("LONG: " + repr(LONG) + " \n")
        # f.write("ELEV: " + repr(ELEV) + " \n")
        # f.write("TAV: " + repr(TAV) + " \n")
        # f.write("AMP: " + repr(AMP) + " \n")
        plt_year = self._Setting.DSSATSetup1.plt_year.getvalue()
        temp = plt_year + '-' + self._Setting.DSSATSetup1.plt_month.getvalue() + '-' + self._Setting.DSSATSetup1.plt_date.getvalue()
        f.write("Planting_Date: " + temp + " \n")
        # f.write("Harvest_Date: " + temp + " + 180 days" + " \n")
        f.write("Harvest_Date: " + temp + " + 170 days" + " \n")  #EJ(4/28/2020)
        BN1 = self._Setting.DSSATSetup1.BN1.getvalue()
        BN2 = self._Setting.DSSATSetup1.BN2.getvalue()
        AN1 = self._Setting.DSSATSetup1.AN1.getvalue()
        AN2 = self._Setting.DSSATSetup1.AN2.getvalue()
        NN1 = repr(100 - float(AN1) - float(BN1))
        NN2 = repr(100 - float(AN2) - float(BN2))
        f.write("SCF_trimester1: " + self._Setting.DSSATSetup1.trimester1.getvalue()[0] + " \n")
        f.write("    BN        : " + BN1 + " \n")
        f.write("    NN        : " + NN1 + " \n")
        f.write("    AN        : " + AN1 + " \n")
        f.write("SCF_trimester2: " + self._Setting.DSSATSetup1.trimester2.cget("text") + " \n")
        f.write("    BN        : " + BN2 + " \n")
        f.write("    NN        : " + NN2 + " \n")
        f.write("    AN        : " + AN2 + " \n")
        if self._Setting.DSSATSetup1.Crop_type.get() == 0:  # Wheat
            crop_type = 'Wheat'
            cultivar = self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0]
            ID_SOIL= self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0]
        elif self._Setting.DSSATSetup1.Crop_type.get() == 1:  # Maize
            crop_type = 'Maize'
            cultivar = self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0]
            ID_SOIL= self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0]
        else:
            crop_type = 'Sorghum'
            cultivar = self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0]
            ID_SOIL= self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0]
        print("cultivar:", cultivar)
        f.write("Crop_type: " + crop_type + " \n")
        f.write("Cultivar: " + cultivar + " \n")
        f.write("Soil_type: " + ID_SOIL + " \n")
        # f.write("Soil_type: " + self._Setting.DSSATSetup1.BN_soil_type.getvalue()[0][
        #                         0:10] + " \n")  # ID_SOIL=self.soil_type.getvalue()[0][0:10]
        f.write("Irrigation_Auto(0)_No(1)_reportedD(2): " + repr(
            self._Setting.DSSATSetup2.rbIrrigation.get()) + " \n")  
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:
            f.write("Managemetn Depth:" + self._Setting.DSSATSetup2.IA_mng_depth.getvalue()+ " \n")
            f.write("Threshold(% of max):" + self._Setting.DSSATSetup2.IA_threshold.getvalue()+ " \n")
            f.write("Efficiency Fraction:" + self._Setting.DSSATSetup2.IA_eff_fraction.getvalue()+ " \n")
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:
            f.write("IR_method: " + self._Setting.DSSATSetup2.IR_method.getvalue()[0][0:5] + " \n")
            #irrigation #1
            if self._Setting.DSSATSetup2.ir_m1.getvalue() != '' and self._Setting.DSSATSetup2.ir_d1.getvalue() != '' and self._Setting.DSSATSetup2.ir_y1.getvalue() != '':
                ir_date = self._Setting.DSSATSetup2.ir_m1.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d1.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y1.getvalue()
                f.write("Irrig Date:" + ir_date + " \n")
                f.write("Irrig Amount:" + self._Setting.DSSATSetup2.ir_amt1.getvalue() + " \n")
            #irrgation #2
            if self._Setting.DSSATSetup2.ir_m2.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y2.getvalue() != '':
                ir_date = self._Setting.DSSATSetup2.ir_m2.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d2.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y2.getvalue()
                f.write("Irrig Date:" + ir_date + " \n")
                f.write("Irrig Amount:" + self._Setting.DSSATSetup2.ir_amt2.getvalue() + " \n")
            #irrigation #3
            if self._Setting.DSSATSetup2.ir_m3.getvalue() != '' and self._Setting.DSSATSetup2.ir_d3.getvalue() != '' and self._Setting.DSSATSetup2.ir_y3.getvalue() != '':
                ir_date = self._Setting.DSSATSetup2.ir_m3.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d3.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y3.getvalue()
                f.write("Irrig Date:" + ir_date + " \n")
                f.write("Irrig Amount:" + self._Setting.DSSATSetup2.ir_amt3.getvalue() + " \n")
            #irrigation #4
            if self._Setting.DSSATSetup2.ir_m4.getvalue() != '' and self._Setting.DSSATSetup2.ir_d4.getvalue() != '' and self._Setting.DSSATSetup2.ir_y4.getvalue() != '':
                ir_date = self._Setting.DSSATSetup2.ir_m4.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d4.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y4.getvalue()
                f.write("Irrig Date:" + ir_date + " \n")
                f.write("Irrig Amount:" + self._Setting.DSSATSetup2.ir_amt3.getvalue() + " \n")
            #irrigation #5
            if self._Setting.DSSATSetup2.ir_m5.getvalue() != '' and self._Setting.DSSATSetup2.ir_d5.getvalue() != '' and self._Setting.DSSATSetup2.ir_y5.getvalue() != '':
                ir_date = self._Setting.DSSATSetup2.ir_m5.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d5.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y5.getvalue()
                f.write("Irrig Date:" + ir_date + " \n")
                f.write("Irrig Amount:" + self._Setting.DSSATSetup2.ir_amt5.getvalue() + " \n")
  #====================================================================
        f.write("Fertilizer_Yes(0)_No(1): " + repr(self._Setting.DSSATSetup2.rbFertApp.get()) + " \n")
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
            if self._Setting.DSSATSetup2.FA_day1.getvalue() != '': 
                f.write("FDATE1: " + self._Setting.DSSATSetup2.FA_day1.getvalue() + " \n")
                f.write("F_material1: " + self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0]+ " \n")
                f.write("F_method1: " + self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0]+ " \n")
                f.write("N_amount1: " + self._Setting.DSSATSetup2.FA_amt_N1.getvalue() + " \n")
                # f.write("P_amount1: " + self._Setting.DSSATSetup2.FA_amt_P1.getvalue() + " \n")
                # f.write("K_amount1: " + self._Setting.DSSATSetup2.FA_amt_K1.getvalue() + " \n")
            if self._Setting.DSSATSetup2.FA_day2.getvalue() != '':
                f.write("FDATE2: " + self._Setting.DSSATSetup2.FA_day2.getvalue() + " \n")
                f.write("F_material2: " + self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0]+ " \n")
                f.write("F_method2: " + self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0]+ " \n")
                f.write("N_amount2: " + self._Setting.DSSATSetup2.FA_amt_N2.getvalue() + " \n")
                # f.write("P_amount2: " + self._Setting.DSSATSetup2.FA_amt_P2.getvalue() + " \n")
                # f.write("K_amount2: " + self._Setting.DSSATSetup2.FA_amt_K2.getvalue() + " \n")
            if self._Setting.DSSATSetup2.FA_day3.getvalue() != '': 
                f.write("FDATE3: " + self._Setting.DSSATSetup2.FA_day3.getvalue() + " \n")
                f.write("F_material3: " + self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0]+ " \n")
                f.write("F_method3: " + self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0]+ " \n")
                f.write("N_amount3: " + self._Setting.DSSATSetup2.FA_amt_N3.getvalue() + " \n")
                # f.write("P_amount3: " + self._Setting.DSSATSetup2.FA_amt_P3.getvalue() + " \n")
                # f.write("K_amount3: " + self._Setting.DSSATSetup2.FA_amt_K3.getvalue() + " \n")
            if self._Setting.DSSATSetup2.FA_day4.getvalue() != '':
                f.write("FDATE4: " + self._Setting.DSSATSetup2.FA_day4.getvalue() + " \n")
                f.write("F_material4: " + self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0]+ " \n")
                f.write("F_method4: " + self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0]+ " \n")
                f.write("N_amount4: " + self._Setting.DSSATSetup2.FA_amt_N4.getvalue() + " \n")
                # f.write("P_amount4: " + self._Setting.DSSATSetup2.FA_amt_P4.getvalue() + " \n")
                # f.write("K_amount4: " + self._Setting.DSSATSetup2.FA_amt_K4.getvalue() + " \n")
            if self._Setting.DSSATSetup2.FA_day5.getvalue() != '':
                f.write("FDATE5: " + self._Setting.DSSATSetup2.FA_day5.getvalue() + " \n")
                f.write("F_material5: " + self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0]+ " \n")
                f.write("F_method5: " + self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0]+ " \n")
                f.write("N_amount5: " + self._Setting.DSSATSetup2.FA_amt_N5.getvalue() + " \n")
                # f.write("P_amount5: " + self._Setting.DSSATSetup2.FA_amt_P5.getvalue() + " \n")
                # f.write("K_amount5: " + self._Setting.DSSATSetup2.FA_amt_K5.getvalue() + " \n")
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':
        #     f.write("Phosphorous simualtion: " + self._Setting.DSSATSetup2.P_button.getvalue()[0] + " \n")
        #     f.write("Extractrable P: " + self._Setting.DSSATSetup2.extP.getvalue()[0] + " \n")
        f.close()

    # =============================================
    # Write SCF information in the Page 1 into *.csv file to feed to WGEN
    # =============================================
    def write_SCF_csv(self):
        # define a dictionary
        scf_ind_dic = {
            " ": -99,
            "JFM": 12,
            "FMA": 1,
            "MAM": 2,
            "AMJ": 3,
            "MJJ": 4,
            "JJA": 5,
            "JAS": 6,
            "ASO": 7,
            "SON": 8,
            "OND": 9,
            "NDJ": 10,
            "DJF": 11}
        trimester = self._Setting.DSSATSetup1.trimester1.getvalue()[0]
        loc_index = scf_ind_dic.get(trimester)
        rowBN = np.full((1, 24), 33.0)
        rowNN = np.full((1, 24), 34.0)
        rowAN = np.full((1, 24), 33.0)
        rowSCF = np.full((1, 24), 0)
        BN1 = self._Setting.DSSATSetup1.BN1.getvalue()
        BN2 = self._Setting.DSSATSetup1.BN2.getvalue()
        AN1 = self._Setting.DSSATSetup1.AN1.getvalue()
        AN2 = self._Setting.DSSATSetup1.AN2.getvalue()
        for i in range(3):
            rowBN[0, loc_index + i] = BN1
            rowBN[0, loc_index + i + 3] = BN2
            rowAN[0, loc_index + i] = AN1
            rowAN[0, loc_index + i + 3] = AN2
            rowNN[0, loc_index + i] = 100 - float(AN1) - float(BN1)
            rowNN[0, loc_index + i + 3] = 100 - float(AN2) - float(BN2)
            rowSCF[0, loc_index + i] = 1
            rowSCF[0, loc_index + i + 3] = 2

        temp_csv = path.join(self._Setting.ScenariosSetup.Working_directory, "SCF_input_2yrs.csv")
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                   quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(
                ['Month', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '1', '2', '3', '4', '5', '6',
                 '7', '8', '9', '10', '11', '12'])
            scfwriter.writerow(['BN'] + rowBN.tolist()[0])
            scfwriter.writerow(['NN'] + rowNN.tolist()[0])
            scfwriter.writerow(['AN'] + rowAN.tolist()[0])
            scfwriter.writerow(['SCF'] + rowSCF.tolist()[0])

    def writeSNX_main_hist(self, SNX_fname, crop):
        if crop == 'WH':
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETWHTEMP.SNX")
        elif crop == 'MZ':
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETMZTEMP.SNX")
        else:  # SG
            temp_snx = path.join(self._Setting.ScenariosSetup.Working_directory, "ETSGTEMP.SNX")
        fr = open(temp_snx, "r")  # opens temp SNX file to read
        fw = open(SNX_fname, "w")  # opens SNX file to write

        # ===set up parameters
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
            MI = '1'  # should be one character
        else:
            MI = '0' 
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
            MF = '1'
        else:  # no fertilizer
            MF = '0'

        if crop == 'WH':   # drybean
            INGENO = self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0][0:6]  # "CI0001 P30F35"
            CNAME = self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0][7:]  # "CI0001 P30F35"
            ID_SOIL = self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.WH_plt_density.getvalue()  #planting density
            i_NO3=self._Setting.DSSATSetup1.WH_ini_NO3.getvalue()[0][0:1]
            # i_NO3 = 'L'  # by default, low initial NO3
            IC_w_ratio = float(self._Setting.DSSATSetup1.WH_ini_H2O.getvalue()[0][0:3])
        elif crop == 'MZ':  # Maize
            INGENO = self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0][0:6]  
            CNAME = self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0][7:]  
            ID_SOIL = self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.MZ_plt_density.getvalue()  #planting density
            i_NO3 = self._Setting.DSSATSetup1.MZ_ini_NO3.getvalue()[0][0:1]  # self.label_04.cget("text")[0:1]  #self.NO3_soil.getvalue()[0][0:1] #'H' #or 'L'
            IC_w_ratio = float(self._Setting.DSSATSetup1.MZ_ini_H2O.getvalue()[0][0:3])
        else: #sorghum
            INGENO = self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][0:6]  
            CNAME = self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][7:]  
            ID_SOIL = self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0][0:10]
            PPOP = self._Setting.DSSATSetup1.SG_plt_density.getvalue()  #planting density
            i_NO3 = self._Setting.DSSATSetup1.SG_ini_NO3.getvalue()[0][0:1]  # self.label_04.cget("text")[0:1]  #self.NO3_soil.getvalue()[0][0:1] #'H' #or 'L'
            IC_w_ratio = float(self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0][0:3])

        WSTA = self._Setting.DSSATSetup1.WStation.getvalue()[0][0:4]  # 'UYTT'
        # ============
        # find the first year in *.WTD & compute how many years are available
        # => then determine IDATE, PDATE and NYERS
        WTD_fname = path.join(self._Setting.ScenariosSetup.Working_directory, WSTA + ".WTD")
        # 1) read observed weather *.WTD (skip 1st row - heading)
        data1 = np.loadtxt(WTD_fname, skiprows=1)
        # convert numpy array to dataframe
        WTD_df = pd.DataFrame({'YEAR': data1[:, 0].astype(int) // 1000,  # python 3.6: / --> //
                               'DOY': data1[:, 0].astype(int) % 1000,
                               'SRAD': data1[:, 1],
                               'TMAX': data1[:, 2],
                               'TMIN': data1[:, 3],
                               'RAIN': data1[:, 4]})
        # convert year-month-date to DOY
        plt_year = self._Setting.DSSATSetup1.plt_year.getvalue()
        temp = plt_year + '-' + self._Setting.DSSATSetup1.plt_month.getvalue() + '-' + self._Setting.DSSATSetup1.plt_date.getvalue()
        plt_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
        if plt_doy == 1:
            if calendar.isleap(WTD_df.YEAR.values[0]):
                IC_date = WTD_df.YEAR.values[0] * 1000 + 366  # YYYYDOY integer
            else:
                IC_date = WTD_df.YEAR.values[0] * 1000 + 365  # YYYYDOY integer
            first_year = WTD_df.YEAR.values[0]
            PDATE = repr(first_year + 1)[2:] + repr(plt_doy).zfill(3)
        else:
            first_year = WTD_df.YEAR[WTD_df["DOY"] == (plt_doy - 1)].values[0]
            IC_date = first_year * 1000 + (plt_doy - 1)
            PDATE = repr(first_year)[2:] + repr(plt_doy).zfill(3)
        ICDAT = repr(IC_date)[2:]
        hv_doy = plt_doy + 210  # tentative harvest date => long enough considering delayed growth
        # adjust hv_date if harvest moves to a next year
        if hv_doy > 365:
            hv_doy = hv_doy - 365
        last_year = WTD_df.YEAR[WTD_df["DOY"] == (hv_doy)].values[-1]
        NYERS = repr(last_year - first_year + 1)
        # ============

        SNH4 = 1.5  # **EJ(5/27/2015) followed by Walter's UYPNDSS6.SNX
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied
            #EJ(10/20) determine fertilizer info 
            fert_count = 0
            FDATE = []
            FMCD = []
            FACD = []
            FDEP = []
            FAMN = []
            FAMP = []
            FAMK = []
            if self._Setting.DSSATSetup2.FA_day1.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth1.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day1.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0][0:5])  # application method     
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth1.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N1.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N1.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P1.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P1.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K1.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K1.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day2.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth2.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day2.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth2.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N2.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N2.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P2.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P2.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K2.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K2.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day3.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth3.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day3.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0][0:5])  # application method     
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth3.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N3.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N3.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P3.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P3.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K3.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K3.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day4.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth4.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day4.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth4.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N4.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N4.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P4.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P4.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K4.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K4.getvalue())
                            fert_count = fert_count +1
            if self._Setting.DSSATSetup2.FA_day5.getvalue() != '':  #days after planting
                if self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0] != 'None':  #material
                    if self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0] != 'None': #method
                        if self._Setting.DSSATSetup2.FA_amt_depth5.getvalue() != '': #depth
                            FDATE.append(self._Setting.DSSATSetup2.FA_day5.getvalue())
                            FMCD.append(self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0][0:5])  # fertilizer material
                            FACD.append(self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0][0:5])  # application method    
                            FDEP.append(self._Setting.DSSATSetup2.FA_amt_depth5.getvalue())
                            # i = 5 if a > 7 else 0  or 'true' if True else 'false'
                            FAMN.append('-99') if self._Setting.DSSATSetup2.FA_amt_N5.getvalue() == '' else FAMN.append(self._Setting.DSSATSetup2.FA_amt_N5.getvalue())
                            FAMP.append('-99') #if self._Setting.DSSATSetup2.FA_amt_P5.getvalue() == '' else FAMP.append(self._Setting.DSSATSetup2.FA_amt_P5.getvalue())
                            FAMK.append('-99') #if self._Setting.DSSATSetup2.FA_amt_K5.getvalue() == '' else FAMK.append(self._Setting.DSSATSetup2.FA_amt_K5.getvalue())
                            fert_count = fert_count +1

        SDATE = ICDAT  # EJ(3/5/2020) #simuation starting date

        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
            IRRIG = 'R'
            IMETH = self._Setting.DSSATSetup2.IR_method.getvalue()[0][0:5]  # 'IR001' #irrigation method
        elif self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  # automatic when required
            IRRIG = 'A'  # automatic, or 'N' (no irrigation)
            IMDEP = self._Setting.DSSATSetup2.IA_mng_depth.getvalue()
            ITHRL = self._Setting.DSSATSetup2.IA_threshold.getvalue()
            IREFF = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue()
        else:
            IRRIG = 'N'  # automatic, or 'N' (no irrigation)
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied
            FERTI = 'D'  # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
        else:
            FERTI = 'N'
        # ===end of setting up paramters

        # read lines 1-9 from temp file
        for line in range(0, 14):
            temp_str = fr.readline()
            fw.write(temp_str)

        # write *TREATMENTS
        # CU='1'
        # #EJ(10/22/2020) Addd Soil Analysis section if P is simulated
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':
        #     SA = '1' 
        # else:
        #     SA = '0'
        SA = '0'
        IC = '1'
        MP = '1'
        MR = '0'
        MC = '0'
        MT = '0'
        ME = '0'
        MH = '0'
        SM = '1'
        temp_str = fr.readline()
        FL = '1'
        fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(
            FL.rjust(3), '1 0 0 ERiMA DCC1                 1',
            FL.rjust(3), SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3),
            MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))
        fw.write(" \n")

        # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)

        # write *CULTIVARS
        temp_str = fr.readline()
        new_str = temp_str[0:3] + crop + temp_str[5:6] + INGENO + temp_str[12:13] + CNAME
        fw.write(new_str)
        fw.write(" \n")

        # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)

        # ================write *FIELDS
        # Get soil info from *.SOL
        SOL_file = path.join(self._Setting.ScenariosSetup.Working_directory, "ET.SOL")
        # soil_depth, wp, fc, nlayer = get_soil_IC(SOL_file, ID_SOIL)
        soil_depth, wp, fc, nlayer, SLTX = get_soil_IC(SOL_file, ID_SOIL)
        SLDP = repr(soil_depth[-1])
        ID_FIELD = WSTA + '0001'
        WSTA_ID =  WSTA
        fw.write(
            '{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5),
                                                                            '       -99   -99   -99   -99   -99   -99 ',
                                                                            SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,
                                                                            ' -99'))
        fw.write(" \n")
        temp_str = fr.readline()  # 1 -99      CCER       -99   -99 DR000   -99   -99
        temp_str = fr.readline()  # @L ...........XCRD ...........YCRD .....ELEV
        fw.write(temp_str)
        temp_str = fr.readline()  # 1             -99             -99       -99   ==> skip
        # ================write *FIELDS - second section
        fw.write('{0:3s}{1:89s}'.format(FL.rjust(3),
                                        '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
        fw.write(" \n")
        fw.write(" \n")

        # #EJ(10/22/2020) Addd Soil Analysis section if P is simulated
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':
        #     fw.write('*SOIL ANALYSIS'+ "\n")
        #     fw.write('@A SADAT  SMHB  SMPX  SMKE  SANAME'+ "\n")
        #     fw.write(' 1 '+ ICDAT + ' SA011 SA001 SA014  -99'+ "\n")
        #     fw.write('@A  SABL  SADM  SAOC  SANI SAPHW SAPHB  SAPX  SAKE  SASC'+ "\n")
        #     soil_depth, SADM, SAOC, SANI, SAPHW = get_soil_SA(SOL_file, ID_SOIL)
        #     if self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'V':  #very low
        #         SAPX = '   2.0'
        #     elif self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'L':  #Low
        #         SAPX = '   7.0'
        #     elif self._Setting.DSSATSetup2.extP.getvalue()[0][0:1] == 'M':  #Medium
        #         SAPX = '  12.0'
        #     else:   #high
        #         SAPX = '  18.0'
        #     for i in range(0, len(soil_depth)):
        #         new_str = ' 1'+ repr(soil_depth[i]).rjust(6) + repr(SADM[i]).rjust(6) + repr(SAOC[i]).rjust(6) + repr(SANI[i]).rjust(6) + repr(SAPHW[i]).rjust(6)+ '   -99' + SAPX + '   -99   -99'+"\n"
        #         fw.write(new_str)

        # read lines from temp file
        for line in range(0, 3):
            temp_str = fr.readline()
            fw.write(temp_str)
        # write *INITIAL CONDITIONS
        temp_str = fr.readline()
        new_str = temp_str[0:9] + ICDAT + temp_str[14:]
        fw.write(new_str)
        temp_str = fr.readline()  # @C  ICBL  SH2O  SNH4  SNO3
        fw.write(temp_str)

        # #Get soil info from *.SOL
        # SOL_file=path.join(self._Setting.ScenariosSetup.Working_directory, "CC.SOL")
        # soil_depth, wp, fc, nlayer = get_soil_IC(SOL_file, ID_SOIL)
        temp_str = fr.readline()
        for nline in range(0, nlayer):
            if nline == 0:  # first layer
                temp_SH2O = IC_w_ratio * (fc[nline] - wp[nline]) + wp[nline]  # EJ(6/25/2015): initial AWC=70% of maximum AWC
                # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3 = '15'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga1':
                    #     SNO3 = '14'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCTolima01' or ID_SOIL == 'CCEspi2014':
                    #     SNO3 = '13'  # **EJ(3/2/2017
                    # else:
                    #     SNO3 = '15'  # **EJ(3/2/2017
                else:  # i_NO3 == 'L':
                    SNO3 = '5'  # **EJ(5/27/2015)
            elif nline == 1:  # second layer
                temp_SH2O = IC_w_ratio * (fc[nline] - wp[nline]) + wp[nline]  # EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3 = '2'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga0':
                    #     SNO3 = '0.5'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCCienaga2':
                    #     SNO3 = '1.4'
                    # elif ID_SOIL == 'CCBuga2014':
                    #     SNO3 = '2.7'
                    # else:
                    #     SNO3 = '0.0'
                        ##self.soiltype_err.activate()
                else:  # elif i_NO3 == 'L':
                    SNO3 = '.5'  # **EJ(4/29/2020) used one constant number regardless of soil types
                    # if ID_SOIL == 'CCCienaga0':
                    #     SNO3 = '0.3'  # **EJ(3/2/2017
                    # elif ID_SOIL == 'CCCienaga2':
                    #     SNO3 = '0.6'
                    # elif ID_SOIL == 'CCBuga2014':
                    #     SNO3 = '1.3'
                    # else:
                    #     SNO3 = '0.0'
            else:
                temp_SH2O = fc[nline]  # float
                SNO3 = '0'  # **EJ(5/27/2015)
            SH2O = repr(temp_SH2O)[0:5]  # convert float to string
            new_str = temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22] + SNO3.rjust(4) + "\n"
            fw.write(new_str)
        fw.write("  \n")

        for nline in range(0, 10):
            temp_str = fr.readline()
            # print temp_str
            if temp_str[0:9] == '*PLANTING':
                break

        fw.write(temp_str)  # *PLANTING DETAILS
        temp_str = fr.readline()  # @P PDATE EDATE
        fw.write(temp_str)

        # write *PLANTING DETAILS
        temp_str = fr.readline()
        PPOE = PPOP #planting density 
        new_str = temp_str[0:3] + PDATE + '   -99' + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
        fw.write(new_str)
        fw.write("  \n")

        # write *IRRIGATION AND WATER MANAGEMENT, if irrigation on reported dates
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  
            fw.write('*IRRIGATION AND WATER MANAGEMENT'+ "\n")
            fw.write('@I  EFIR  IDEP  ITHR  IEPT  IOFF  IAME  IAMT IRNAME'+ "\n")
            fw.write(' 1     1    30    50   100 GS000 IR001    10 -99'+ "\n")
            fw.write('@I IDATE  IROP IRVAL'+ "\n")
            IROP = self._Setting.DSSATSetup2.IR_method.getvalue()[0][0:5]  #irrigation method
            #irrgation appliation 1
            if self._Setting.DSSATSetup2.ir_m1.getvalue() != '' and self._Setting.DSSATSetup2.ir_d1.getvalue() != '' and self._Setting.DSSATSetup2.ir_y1.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt1.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y1.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m1.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d1.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt1.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 2
            if self._Setting.DSSATSetup2.ir_m2.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y2.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt2.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y2.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m2.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d2.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt2.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m3.getvalue() != '' and self._Setting.DSSATSetup2.ir_d3.getvalue() != '' and self._Setting.DSSATSetup2.ir_y3.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt3.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y3.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m3.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d3.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt3.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m4.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y4.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt4.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y4.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m4.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d4.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt4.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #irrgation appliation 3
            if self._Setting.DSSATSetup2.ir_m5.getvalue() != '' and self._Setting.DSSATSetup2.ir_d5.getvalue() != '' and self._Setting.DSSATSetup2.ir_y5.getvalue() != '':
                if self._Setting.DSSATSetup2.ir_amt5.getvalue() != '' :
                    # convert year-month-date to DOY
                    irr_year = self._Setting.DSSATSetup2.ir_y5.getvalue()
                    temp = irr_year + '-' + self._Setting.DSSATSetup2.ir_m5.getvalue() + '-' + self._Setting.DSSATSetup2.ir_d5.getvalue()
                    irr_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
                    IDATE = irr_year[2:] + repr(irr_doy).zfill(3)
                    IRVAL = self._Setting.DSSATSetup2.ir_amt5.getvalue()
                    fw.write(' 1 '+IDATE + ' ' + IROP + IRVAL.rjust(6)+ "\n")
            #end of writing irrigation application

        # write *FERTILIZERS (INORGANIC)
        if self._Setting.DSSATSetup2.rbFertApp.get() == 0:  # fertilizer applied
            if fert_count > 0:   # fertilizer applied
                # read lines from temp file
                for line in range(0, 3):
                    temp_str = fr.readline()
                    fw.write(temp_str)
                temp_str = fr.readline()
                for i in range(fert_count):
                    new_str = temp_str[0:5] + FDATE[i].rjust(3) + ' ' + FMCD[i].rjust(5) + ' ' + FACD[i].rjust(5) + ' ' + FDEP[i].rjust(5) + ' ' + FAMN[i].rjust(5) + ' ' + FAMP[i].rjust(5) + ' ' + FAMK[i].rjust(5) + temp_str[44:]
                    fw.write(new_str)
                    fw.write(" \n")
                temp_str = fr.readline()
        fw.write("  \n")
        for nline in range(0, 10):
            temp_str = fr.readline()
            # print temp_str
            if temp_str[0:11] == '*SIMULATION':
                break
        fw.write(temp_str)  # *SIMULATION CONTROLS
        temp_str = fr.readline()
        fw.write(temp_str)  # @N GENERAL     NYERS NREPS START SDATE RSEED SNAME
        # write *SIMULATION CONTROLS
        temp_str = fr.readline()
        new_str = temp_str[0:18] + NYERS.rjust(2) + temp_str[20:33] + SDATE + temp_str[38:]
        fw.write(new_str)
        temp_str = fr.readline()  # @N OPTIONS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 OP
        # if self._Setting.DSSATSetup2.P_button.getvalue()[0][0:1] == 'Y':  #if phosphorous simulation is "on"
        #     fw.write(' 1 OP              Y     Y     Y     Y     N     N     N     N     D'+ "\n")
        # else:
        #     fw.write(' 1 OP              Y     Y     Y     N     N     N     N     N     D'+ "\n")
        fw.write(' 1 OP              Y     Y     Y     N     N     N     N     N     D'+ "\n")
        temp_str = fr.readline()  # @N METHODS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 ME
        fw.write(temp_str)
        temp_str = fr.readline()  # @N MANAGEMENT
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 MA
        new_str = temp_str[0:25] + IRRIG + temp_str[26:31] + FERTI + temp_str[32:]
        fw.write(new_str)
        temp_str = fr.readline()  # @N OUTPUTS
        fw.write(temp_str)
        temp_str = fr.readline()  # 1 OU
        fw.write(temp_str)

        # read lines from temp file
        for line in range(0, 5):
            temp_str = fr.readline()
            fw.write(temp_str)
        # irrigation method
        temp_str = fr.readline()  # 1 IR
        if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  # automatic when required
            IMDEP = self._Setting.DSSATSetup2.IA_mng_depth.getvalue()
            ITHRL = self._Setting.DSSATSetup2.IA_threshold.getvalue()
            IREFF = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue()
            fw.write(' 1 IR          ' + IMDEP.rjust(5) + ITHRL.rjust(6) + '   100 GS000 IR001    10'+ IREFF.rjust(6)+ "\n")
        else:
            # new_str = temp_str[0:39] + IMETH + temp_str[44:]
            fw.write(temp_str)

        # read lines from temp file
        for line in range(0, 7):
            temp_str = fr.readline()
            fw.write(temp_str)

        fr.close()
        fw.close()

    # ====End of WRITE *.SNX

    def writeV47_main_hist(self, sname, crop):  # sname includes full path
        sname = sname.replace("/", "\\")
        if crop == 'WH':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_WH.V47")
        elif crop == 'MZ':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_MZ.V47")
        else:  ##'SG':
            temp_dv7 = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch_template_SG.V47")

        dv7_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "DSSBatch.V47")
        fr = open(temp_dv7, "r")  # opens temp DV4 file to read
        fw = open(dv7_fname, "w")
        # read template and write lines
        for line in range(0, 10):
            temp_str = fr.readline()
            fw.write(temp_str)

        temp_str = fr.readline()
        new_str2 = '{0:<95}{1:4s}'.format(sname, repr(1).rjust(4)) + temp_str[99:]
        fw.write(new_str2)

        fr.close()
        fw.close()


# =============================================
# =============================================
def get_soil_IC(SOL_file, ID_SOIL):
    # SOL_file=Wdir_path.replace("/","\\") + "\\SN.SOL"
    # initialize
    depth_layer = []
    ll_layer = []
    ul_layer = []
    n_layer = 0
    soil_flag = 0
    count = 0
    fname = open(SOL_file, "r")  # opens *.SOL
    for line in fname:
        if ID_SOIL in line:
            soil_depth = line[33:38] #37]
            s_class = line[25:29]
            soil_flag = 1
        if soil_flag == 1:
            count = count + 1
            if count >= 7:
                depth_layer.append(int(line[0:6]))
                ll_layer.append(float(line[13:18]))
                ul_layer.append(float(line[19:24]))
                n_layer = n_layer + 1
                if line[3:6].strip() == soil_depth.strip():
                    # print(depth_layer)
                    # print(line[3:6])
                    # print(s_class)
                    # yield depth_layer
                    # yield ll_layer
                    # yield ul_layer
                    # yield n_layer
                    # yield s_class
                    fname.close()
                    break
    return depth_layer, ll_layer, ul_layer, n_layer, s_class
# =============================================
# =============================================
def get_soil_SA(SOL_file, ID_SOIL):
    # SOL_file=Wdir_path.replace("/","\\") + "\\CC.SOL"
    # initialize
    depth_layer = []
    SADM = [] #bulk density
    SAOC = [] #organic carbon %
    SANI = [] #total nitrogen %
    SAPHW = [] #pH in water
    soil_flag = 0
    count = 0
    fname = open(SOL_file, "r")  # opens *.SOL
    for line in fname:
        if ID_SOIL in line:
            soil_depth = line[33:38] #37]
            soil_flag = 1
        if soil_flag == 1:
            count = count + 1
            if count >= 7:
                depth_layer.append(int(line[0:6]))
                SADM.append(float(line[43:49]))
                SAOC.append(float(line[49:55]))
                SANI.append(float(line[73:79]))
                SAPHW.append(float(line[79:85]))
                if line[3:6].strip() == soil_depth.strip():
                    fname.close()
                    break
    return depth_layer, SADM, SAOC, SANI, SAPHW
