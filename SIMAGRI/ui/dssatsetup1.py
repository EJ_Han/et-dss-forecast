  # -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *

# ==========================Second page "DSSAT Setup 1"
class DSSATSetup1UI:

  __UI_Name__  = loc.DSSATSetup1.Title
  _UIParent = None
  _UINootbook = None

  _Setting = None

  def __init__(self, setting, parent, notebook):
    print("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

    # =========================Third page for "DSSAT baseline setup - I "
    page3 = notebook.add(self.__UI_Name__)
    notebook.tab(self.__UI_Name__).focus_set()

    # 1) ADD SCROLLED FRAME
    sf_p1 = Pmw.ScrolledFrame(page3)  # ,usehullsize=1, hull_width = 700, hull_height=220)
    sf_p1.pack(padx = 5, pady = 3, fill = 'both', expand = YES)
    
      # set up "weather station" #CHANGEFORSENEGAL
    group11 = Pmw.Group(sf_p1.interior(), tag_text = 'Seasonal Climate Forecasts',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
      #assign frame 1
    fm11=Frame(group11.interior())      
    Wstation_list = ("MELK(Melkasa)", "BAKO(Baco)", "AWAS(Awassa)", "KOBO(Kobbo)", 
                     "KULU(Kulumsa)", "MEIS(Meiso)", "ASEL(Assela)", "MAHO(Mahoni)")
    self._Setting.DSSATSetup1.WStation = Pmw.ComboBox(fm11, label_text='*Nearest Weather Station:', labelpos='wn',
                    listbox_width=20, dropdown=1,
                    #selectioncommand = self.AvailableYears,
                    scrolledlist_items=Wstation_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WStation.selectitem(Wstation_list[0])
    self._Setting.DSSATSetup1.WStation.pack(fill = 'x', side=LEFT,padx = 10, pady = 5)
    fm11.pack(fill='x', expand=1,padx=10, side=TOP)

    fm12=Frame(group11.interior())         
    self.label11 = Label(fm12, text='Seasonal Climate Forecasts',padx=5, pady=5)
    self.label11.grid(row=0,column=0, sticky=W) #rowspan=1,columnspan=1)
    self.label12 = Label(fm12, text='1st trimester:',padx=5, pady=5)
    self.label12.grid(row=2,column=0, sticky=E) #rowspan=1,columnspan=1)
    self.label13 = Label(fm12, text='2nd trimester:',padx=5, pady=5)
    self.label13.grid(row=3,column=0, sticky=E) #rowspan=1,columnspan=1)
    self.label14 = Label(fm12, text='Trimester',padx=5, pady=5)
    self.label14.grid(row=1,column=1, sticky=W) #rowspan=1,columnspan=1)
    self.label15 = Label(fm12, text='Above-Normal(%)',padx=5, pady=5)
    self.label15.grid(row=1,column=2) #rowspan=1,columnspan=1)
    self.label16 = Label(fm12, text='Below-Normal(%)',padx=5, pady=5)
    self.label16.grid(row=1,column=3) #rowspan=1,columnspan=1)
    self.label17 = Label(fm12, text='Near-Normal(%)',padx=5, pady=5)
    self.label17.grid(row=1,column=4) #rowspan=1,columnspan=1)
    trimester_list = (" ","JFM", "FMA", "MAM", "AMJ", "MJJ", "JJA", "JAS", "ASO", "SON","OND", "NDJ", "DJF")
    self._Setting.DSSATSetup1.trimester1 = Pmw.ComboBox(fm12, dropdown=1, 
                    selectioncommand = self.FillTrimester2,
                    scrolledlist_items=trimester_list,
                    entryfield_entry_state=DISABLED)   #listbox_width=20, dropdown=1, 
    # self._Setting.DSSATSetup1.trimester1.selectitem(trimester_list[6])  #(trimester_list[0])
    self._Setting.DSSATSetup1.trimester1.grid(row=2,column=1, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.trimester2 = Label(fm12, text='N/A',relief='sunken',width=20)
    self._Setting.DSSATSetup1.trimester2.grid(row=3,column=1, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.NN1 = Label(fm12, text='N/A',relief='sunken',width=14)
    self._Setting.DSSATSetup1.NN1.grid(row=2,column=4, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.NN2 = Label(fm12, text='N/A',relief='sunken',width=14)
    self._Setting.DSSATSetup1.NN2.grid(row=3,column=4, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.AN1 = Pmw.EntryField(fm12,
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 99, 'minstrict' : 0})
    self._Setting.DSSATSetup1.AN1.grid(row=2,column=2, sticky=W) 
    self._Setting.DSSATSetup1.AN2 = Pmw.EntryField(fm12,
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 99, 'minstrict' : 0})
    self._Setting.DSSATSetup1.AN2.grid(row=3,column=2, sticky=W) 
    self._Setting.DSSATSetup1.BN1 = Pmw.EntryField(fm12,
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 99, 'minstrict' : 0},
        modifiedcommand = self.fill_NN1)
    self._Setting.DSSATSetup1.BN1.grid(row=2,column=3, sticky=W) 
    self._Setting.DSSATSetup1.BN2 = Pmw.EntryField(fm12,
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 99, 'minstrict' : 0},
        modifiedcommand = self.fill_NN2)
    self._Setting.DSSATSetup1.BN2.grid(row=3,column=3, sticky=W) 
    fm12.pack(fill='x', expand=1,padx=10,side=TOP)  

    # set up "Planting date"
    group12 = Pmw.Group(sf_p1.interior(), tag_text = 'Planting Date',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.plt_year = Pmw.EntryField(group12.interior(), labelpos = 'w',
        label_text = 'Year:',
        validate = {'validator': 'numeric'})
    self._Setting.DSSATSetup1.plt_year.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    # self._Setting.DSSATSetup1.plt_year = Pmw.EntryField(group12.interior(), labelpos = 'w',
    #     label_text = 'Year:',
    #     validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2050, 'minstrict' : 1950})
    # self._Setting.DSSATSetup1.plt_year.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)    
    self._Setting.DSSATSetup1.plt_month = Pmw.EntryField(group12.interior(), labelpos = 'w',
        label_text = 'Month:',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1})
    self._Setting.DSSATSetup1.plt_month.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)

    self._Setting.DSSATSetup1.plt_date = Pmw.EntryField(group12.interior(), labelpos = 'w',
        label_text = 'Day:',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1})
    self._Setting.DSSATSetup1.plt_date.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)

    # self._Setting.DSSATSetup1.plt_year.setentry("2010")  #!!!!!!!!!!!!!!!!!!=TEMPORARY    
    self._Setting.DSSATSetup1.plt_month.setentry("7")  #!!!!!!!!!!!!!!!!!!=TEMPORARY   
    self._Setting.DSSATSetup1.plt_date.setentry("19")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  
    self._Setting.DSSATSetup1.AN1.setentry("33")  #!!!!!!!!!!!!!!!!!!=TEMPORARY    
    self._Setting.DSSATSetup1.AN2.setentry("33")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  
    self._Setting.DSSATSetup1.BN1.setentry("33")  #!!!!!!!!!!!!!!!!!!=TEMPORARY    
    self._Setting.DSSATSetup1.BN2.setentry("33")  #!!!!!!!!!!!!!!!!!!=TEMPORARY   

    # select Crop type (Drybean, Maize, Sorghum)
    group13 = Pmw.Group(sf_p1.interior(), tag_text = 'Crop to Plant',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group13.pack(fill='both', expand=1, side=TOP, padx = 10, pady = 5)    
  
      # Radio button to select "crop type"
    self._Setting.DSSATSetup1.Crop_type = tkinter.IntVar()  # Rbutton2
    # Crop_option = [('Drybean', 0), ('Maize', 1), ('Sorghum', 2)]
    Crop_option = [('Wheat', 0), ('Maize', 1), ('Sorghum', 2)]
    # Crop_option = [('Drybean', 0), ('Maize', 1)]
    for text, value in Crop_option:
      Radiobutton(group13.interior(), text = text, command = self.empty_crop_selection_label,
          value = value, variable = self._Setting.DSSATSetup1.Crop_type).pack(side = LEFT, expand = YES)
    self._Setting.DSSATSetup1.Crop_type.set(1)   #By default- Drybean

    # Create button to launch the dialog 
    crop_button=tkinter.Button(sf_p1.interior(),
            text = 'Click to add more details for the selected crop',
            command = self.getCropInput,bg='gray70').pack(side=TOP,anchor=N)

    # Copy DSSAT input from cultivar dialog
    frame_13 =Frame(sf_p1.interior())
    label_1 = Label(frame_13, text='Cultivar Type:', padx=5, pady=5)
    label_1.grid(row=0,column=0,sticky=E)
    self.label_01 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=15) 
    self.label_01.grid(row=0,column=1,sticky=W)
    label_2 = Label(frame_13, text='Soil Type:', padx=5, pady=5)
    label_2.grid(row=0,column=2,sticky=E)
    self.label_02 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=15) 
    self.label_02.grid(row=0,column=3,sticky=W)
    label_3 = Label(frame_13, text='Initial H2O:', padx=5, pady=5)
    label_3.grid(row=1,column=0,sticky=E)
    self.label_03 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=15)  
    self.label_03.grid(row=1,column=1,sticky=W)
    label_4 = Label(frame_13, text='Initial NO3:', padx=5, pady=5)
    label_4.grid(row=1,column=2,sticky=E)
    self.label_04 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=15) 
    self.label_04.grid(row=1,column=3,sticky=W)
    label_5 = Label(frame_13, text='Planting Density [plants/m2]):', padx=5, pady=5)
    label_5.grid(row=2,column=2,sticky=E)
    self.label_05 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=12)  
    self.label_05.grid(row=2,column=3,sticky=W)
    frame_13.pack(fill = 'x', expand = 1,side=TOP)   

    # Init. Dialogs
    self.__initDialog()

  def __initDialog(self):
    #Dialog to get specific input for Wheat
    self.crop_dialog_WH = Pmw.Dialog(self._UIParent, title='Input for Drybean')
      # set up "soil information"
    group_c11 = Pmw.Group(self.crop_dialog_WH.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list = ("ETET000010(AWAS,L)","ETET000_10(AWAS,L, shallow)","ETET000011(BAKO,C)","ETET001_11(BAKO,C,shallow)",
                "ETET000018(MELK,L)","ETET001_18(MELK,L,shallow)","ETET000015(KULU,C)","ETET001_15(KULU,C,shallow)",
                "CCETET000022(MIES, C)","ETET001_22(MIES, C, shallow)","ET00510062(ASEL,CL)","ET00510_62(ASEL,CL,shallow)",
                "ET00990066(MAHO,C)","ET00990_66(MAHO,C,shallow)", "ET00920067(KOBO,CL)","ET00920_67(KOBO,CL,shallow)""None")
    self._Setting.DSSATSetup1.WH_soil_type = Pmw.ComboBox(group_c11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WH_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.WH_soil_type.selectitem(soil_list[2])

    # set up "cultivar selection"
    group_c12 = Pmw.Group(self.crop_dialog_WH.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list = ("CI2021 KT-KUB", "CI2022 RMSI ", "CI2023 Meda wolabu", "CI2024 Sofumer ", "CI2025 Hollandi ", "None")
    self._Setting.DSSATSetup1.WH_cul_type = Pmw.ComboBox(group_c12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WH_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.WH_cul_type.selectitem(cul_list[0]) 

    # set up "initial H2O condition"
    group_c13 = Pmw.Group(self.crop_dialog_WH.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    # wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
    #                  "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.WH_ini_H2O = Pmw.ComboBox(group_c13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WH_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.WH_ini_H2O.selectitem(wetness_list[2])

    # set up "initial NO3 condition"
    group_c14 = Pmw.Group(self.crop_dialog_WH.interior(), tag_text = 'Initial soil NO3',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    NO3_list = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    self._Setting.DSSATSetup1.WH_ini_NO3 = Pmw.ComboBox(group_c14.interior(), label_text='Nitrate level:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=NO3_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WH_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.WH_ini_NO3.selectitem(NO3_list[1]) 

    # set up "Planting density"
    group_c15 = Pmw.Group(self.crop_dialog_WH.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.WH_plt_density = Pmw.EntryField(group_c15.interior(), labelpos = 'w', label_text = 'Planting Density:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.WH_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.WH_plt_density.setentry("175")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    self.crop_dialog_WH.withdraw()
    #========end of crop_dialog for Drybean
    
    #Dialog to get specific input for Maize
    self.crop_dialog_MZ = Pmw.Dialog(self._UIParent, title='Input for Maize')
      # set up "soil information"
    group_b11 = Pmw.Group(self.crop_dialog_MZ.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list2 = ("ETET000010(AWAS,L)","ETET000_10(AWAS,L, shallow)","ETET000011(BAKO,C)","ETET001_11(BAKO,C,shallow)",
                "ETET000018(MELK,L)","ETET001_18(MELK,L,shallow)","ETET000015(KULU,C)","ETET001_15(KULU,C,shallow)",
                "CCETET000022(MIES, C)","ETET001_22(MIES, C, shallow)","ET00510062(ASEL,CL)","ET00510_62(ASEL,CL,shallow)",
                "ET00990066(MAHO,C)","ET00990_66(MAHO,C,shallow)", "ET00920067(KOBO,CL)","ET00920_67(KOBO,CL,shallow)""None")
    self._Setting.DSSATSetup1.MZ_soil_type = Pmw.ComboBox(group_b11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.MZ_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.MZ_soil_type.selectitem(soil_list2[0])
    
    # set up "cultivar selection"
    group_b12 = Pmw.Group(self.crop_dialog_MZ.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list2 = ("CIMT01 BH540-Kassie","CIMT02 MELKASA-Kassi","CIMT17 BH660-FAW-40%", "CIMT19 MELKASA2-FAW-40%", "CIMT21 MELKASA-LowY", "None")
    self._Setting.DSSATSetup1.MZ_cul_type = Pmw.ComboBox(group_b12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.MZ_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.MZ_cul_type.selectitem(cul_list2[0])

    # set up "initial H2O condition"
    group_b13 = Pmw.Group(self.crop_dialog_MZ.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    # wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
    #                  "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.MZ_ini_H2O = Pmw.ComboBox(group_b13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.MZ_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.MZ_ini_H2O.selectitem(wetness_list[2])

    # set up "initial NO3 condition"
    group_b14 = Pmw.Group(self.crop_dialog_MZ.interior(), tag_text = 'Initial soil NO3',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    NO3_list2 = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    self._Setting.DSSATSetup1.MZ_ini_NO3 = Pmw.ComboBox(group_b14.interior(), label_text='Nitrate level:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=NO3_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.MZ_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.MZ_ini_NO3.selectitem(NO3_list2[1])    
    # set up "Planting density"
    group_b15 = Pmw.Group(self.crop_dialog_MZ.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.MZ_plt_density = Pmw.EntryField(group_b15.interior(), labelpos = 'w', label_text = 'Planting Density:',validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.MZ_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.MZ_plt_density.setentry("6.6")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    self.crop_dialog_MZ.withdraw()
    #========end of crop_dialog for Maize

    #Dialog to get specific input for Sorghum
    self.crop_dialog_SG = Pmw.Dialog(self._UIParent, title='Input for Sorghum')
      # set up "soil information"
    group_s11 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list3 = ("ETET000010(AWAS,L)","ETET000_10(AWAS,L, shallow)","ETET000011(BAKO,C)","ETET001_11(BAKO,C,shallow)",
                "ETET000018(MELK,L)","ETET001_18(MELK,L,shallow)","ETET000015(KULU,C)","ETET001_15(KULU,C,shallow)",
                "CCETET000022(MIES, C)","ETET001_22(MIES, C, shallow)","ET00510062(ASEL,CL)","ET00510_62(ASEL,CL,shallow)",
                "ET00990066(MAHO,C)","ET00990_66(MAHO,C,shallow)", "ET00920067(KOBO,CL)","ET00920_67(KOBO,CL,shallow)""None")
    self._Setting.DSSATSetup1.SG_soil_type = Pmw.ComboBox(group_s11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list3,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.SG_soil_type.selectitem(soil_list3[0])
    
    # set up "cultivar selection"
    group_s12 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list3 = ("IB0020 ESH-1","IB0020 ESH-2","IB0027 Dekeba","IB0027 Melkam","IB0027 Teshale","None")
    self._Setting.DSSATSetup1.SG_cul_type = Pmw.ComboBox(group_s12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list3,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.SG_cul_type.selectitem(cul_list3[0])

    # set up "initial H2O condition"
    group_s13 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    # wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
    #                  "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.SG_ini_H2O = Pmw.ComboBox(group_s13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.SG_ini_H2O.selectitem(wetness_list[2])

    #set up "initial NO3 condition"
    group_s14 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Initial soil NO3',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    NO3_list2 = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    self._Setting.DSSATSetup1.SG_ini_NO3 = Pmw.ComboBox(group_s14.interior(), label_text='Nitrate level:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=NO3_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.SG_ini_NO3.selectitem(NO3_list2[1])
    # set up "Planting density"
    group_s15 = Pmw.Group(self.crop_dialog_SG.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.SG_plt_density = Pmw.EntryField(group_s15.interior(), labelpos = 'w', label_text = 'Planting Density:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.SG_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.SG_plt_density.setentry("6")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    self.crop_dialog_SG.withdraw()
    #========end of crop_dialog for sorghum


  #Get input for each cultivar (planting date, soil, IC etc)
  def getCropInput(self):
      if self._Setting.DSSATSetup1.Crop_type.get() == 0:  #Wheat
          self.crop_dialog_WH.activate()
          if self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0][0:4]  != "None":
              self.label_01.configure(text=self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                          defaultbutton = 0,
                          message_text = 'No cultivar type is chosen!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
          if self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0][0:4]  != "None":
              self.label_02.configure(text=self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                          defaultbutton = 0,
                          message_text = 'No soil type is chosen!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
          if self._Setting.DSSATSetup1.WH_ini_H2O.getvalue()[0][0:4]  != "None":
              self.label_03.configure(text=self._Setting.DSSATSetup1.WH_ini_H2O.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                          defaultbutton = 0,
                          message_text = 'No initial soil H2O condition is selected!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
          self.label_04.configure(text=self._Setting.DSSATSetup1.WH_ini_NO3.getvalue()[0],background='honeydew1')
          # self.label_04.configure(text='Low(5 ppm NO3)',background='honeydew1')
          self.label_05.configure(text=self._Setting.DSSATSetup1.WH_plt_density.getvalue(),background='honeydew1')
      elif self._Setting.DSSATSetup1.Crop_type.get() == 1: #Maize
        self.crop_dialog_MZ.activate()
        if self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0][0:4]  != "None":
            self.label_01.configure(text=self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0],background='honeydew1')
        else: 
            dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                        defaultbutton = 0,
                        message_text = 'No cultivar type is chosen!')
            dialog.iconname('error message dialog')
            result=dialog.activate()
            print('error message is ', result)
        if self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0][0:4]  != "None":
            self.label_02.configure(text = self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0],background='honeydew1')
        else: 
            dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                        defaultbutton = 0,
                        message_text = 'No soil type is chosen!')
            dialog.iconname('error message dialog')
            result=dialog.activate()
        if self._Setting.DSSATSetup1.MZ_ini_H2O.getvalue()[0][0:4]  != "None":
            self.label_03.configure(text=self._Setting.DSSATSetup1.MZ_ini_H2O.getvalue()[0],background='honeydew1')
        else: 
            dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                        defaultbutton = 0,
                        message_text = 'No initial soil H2O condition is selected!')
            dialog.iconname('error message dialog')
            result=dialog.activate()
        self.label_04.configure(text=self._Setting.DSSATSetup1.MZ_ini_NO3.getvalue()[0],background='honeydew1')
        self.label_05.configure(text=self._Setting.DSSATSetup1.MZ_plt_density.getvalue(),background='honeydew1')
      else:
          self.crop_dialog_SG.activate()
          if self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][0:4]  != "None":
              self.label_01.configure(text=self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                          defaultbutton = 0,
                          message_text = 'No cultivar type is chosen!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
              print('error message is ', result)
          if self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0][0:4]  != "None":
              self.label_02.configure(text = self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                          defaultbutton = 0,
                          message_text = 'No soil type is chosen!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
          if self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0][0:4]  != "None":
              self.label_03.configure(text=self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0],background='honeydew1')
          else: 
              dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                          defaultbutton = 0,
                          message_text = 'No initial soil H2O condition is selected!')
              dialog.iconname('error message dialog')
              result=dialog.activate()
          self.label_04.configure(text=self._Setting.DSSATSetup1.SG_ini_NO3.getvalue()[0],background='honeydew1')
          self.label_05.configure(text=self._Setting.DSSATSetup1.SG_plt_density.getvalue(),background='honeydew1')
          
  def FillTrimester2(self, string):
    #define a dictionary
    trimester_dic = {
      " ":"N/A",
      "JFM":"AMJ",
      "FMA":"MJJ",
      "MAM":"JJA",
      "AMJ":"JAS",
      "MJJ":"ASO",
      "JJA":"SON",
      "JAS":"OND",
      "ASO":"NDJ",
      "SON":"DJF",
      "OND":"JFM",
      "NDJ":"FMA",
      "DJF":"MAM" }
    self._Setting.DSSATSetup1.trimester2.configure(text=trimester_dic.get(string),background='honeydew1')


  def fill_NN1(self):
    temp_AN1 = self._Setting.DSSATSetup1.AN1.getvalue()
    temp_NN = 100-float(temp_AN1) - float(self._Setting.DSSATSetup1.BN1.getvalue())
    self._Setting.DSSATSetup1.NN1.configure(text=repr(temp_NN),background='honeydew1')

  def fill_NN2(self):
    temp_AN2 = self._Setting.DSSATSetup1.AN2.getvalue()
    temp_NN = 100-float(temp_AN2) - float(self._Setting.DSSATSetup1.BN2.getvalue())
    self._Setting.DSSATSetup1.NN2.configure(text=repr(temp_NN),background='honeydew1')  

  def AvailableYears(self, string):
    # if string[0:4] == 'CUCH' or string[0:4] == 'EAMO' or string[0:4] == 'ZAPA':
    #     s_year='1981'
    #     e_year='2018'
    # elif string[0:4] == 'CTUR' or string[0:4] == 'CNAT' or string[0:4] == 'CUNI':
    #     s_year='1980'
    #     e_year='2015'
    # else:   ##NEED TO BE UPDAED LATER
    #     s_year='1980'
    #     e_year='2012'
    s_year='1981'
    e_year='2018'
    self._Setting.DSSATSetup1.avail_year1.configure(text=s_year,background='honeydew1')
    self._Setting.DSSATSetup1.avail_year2.configure(text=e_year,background='honeydew1')

  def empty_crop_selection_label(self):
    self.label_01.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label_02.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label_03.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label_04.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label_05.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')

  # initialize UI (user interface) with Setting
  def initUI_Setting(self):
    # self.empty_cultivar_selection_label()
    self.empty_crop_selection_label()

    if self._Setting.DSSATSetup1.Crop_type.get() == 0:  #Drybean
      self.label_01.configure(text=self._Setting.DSSATSetup1.WH_cul_type.getvalue()[0],background='honeydew1')
      self.label_02.configure(text=self._Setting.DSSATSetup1.WH_soil_type.getvalue()[0],background='honeydew1')
      self.label_03.configure(text=self._Setting.DSSATSetup1.WH_ini_H2O.getvalue()[0],background='honeydew1')
      # self.label_04.configure(text=self._Setting.DSSATSetup1.WH_ini_NO3.getvalue()[0],background='honeydew1')
      self.label_04.configure(text='Low(5 ppm NO3)',background='honeydew1')
      self.label_05.configure(text=self._Setting.DSSATSetup1.WH_plt_density.getvalue(),background='honeydew1')
    elif self._Setting.DSSATSetup1.Crop_type.get() == 1:  #Maize
      self.label_01.configure(text=self._Setting.DSSATSetup1.MZ_cul_type.getvalue()[0],background='honeydew1')
      self.label_02.configure(text=self._Setting.DSSATSetup1.MZ_soil_type.getvalue()[0],background='honeydew1')
      self.label_03.configure(text=self._Setting.DSSATSetup1.MZ_ini_H2O.getvalue()[0],background='honeydew1')
      self.label_04.configure(text=self._Setting.DSSATSetup1.MZ_ini_NO3.getvalue()[0],background='honeydew1')
      self.label_05.configure(text=self._Setting.DSSATSetup1.MZ_plt_density.getvalue(),background='honeydew1')
    else:  #Sorghum
      self.label_01.configure(text=self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0],background='honeydew1')
      self.label_02.configure(text=self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0],background='honeydew1')
      self.label_03.configure(text=self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0],background='honeydew1')
      self.label_04.configure(text=self._Setting.DSSATSetup1.SG_ini_NO3.getvalue()[0],background='honeydew1')
      self.label_05.configure(text=self._Setting.DSSATSetup1.SG_plt_density.getvalue(),background='honeydew1')
