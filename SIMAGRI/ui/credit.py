# -*- coding: utf-8 -*-
"""
Redesigned on Thu December 26 17:28:23 2016
@Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *


#=========================6th  page for "Credit"
class CreditUI:
  __UI_Name__ = loc.Credit.Title

  _UIParent = None
  _UINootbook = None

  _UIMyPage = None

  _Setting = None

  def __init__(self, setting, parent, notebook):
    print("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

#=========================6th  page for "Credit"
    self._UIMyPage = notebook.add(self.__UI_Name__)
    notebook.tab('*CREDIT').focus_set()

   # 1) ADD SCROLLED FRAME
    sf_p6 = Pmw.ScrolledFrame(self._UIMyPage) #,usehullsize=1, hull_width=700, hull_height=220)
    sf_p6.pack(padx = 5, pady = 3, fill='both', expand = YES)

    group61 = Pmw.Group(sf_p6.interior(), tag_text = loc.Credit.Authors)
    group61.pack(fill = 'x',anchor = N, padx = 10, pady = 5)

    self.authrs1 = Label(group61.interior(), text = 'Eunjin Han, Ph.D. at IRI', padx=5, pady=5)
    self.authrs1.pack(side=TOP, anchor=W, expand=YES)

    group62 = Pmw.Group(sf_p6.interior(), tag_text = loc.Credit.Other_Contributions)
    group62.pack(fill = 'x',anchor = N, padx = 10, pady = 5)


    self.cont3 = Label(group62.interior(), text = 'Walter Baethgen, Ph.D. at IRI', padx = 5, pady = 5)
    self.cont3.pack(side = TOP, anchor = W, expand = YES)
    self.cont5 = Label(group62.interior(), text = '‪Jemal Seid Ahmed, at EIAR', padx = 5, pady = 5)
    self.cont5.pack(side = TOP, anchor = W, expand = YES)
    self.cont6 = Label(group62.interior(), text = 'Kindie Tesfaye, Ph.D. at CIMMYT', padx = 5, pady = 5)
    self.cont6.pack(side = TOP, anchor = W, expand = YES)
    self.cont6 = Label(group62.interior(), text = 'Dawit Solomon, Ph.D. at CCAFS', padx = 5, pady = 5)
    self.cont6.pack(side = TOP, anchor = W, expand = YES)
    self.cont1 = Label(group62.interior(), text = 'Seongkyu Lee, Ph.D. at APCC', padx = 5, pady = 5)
    self.cont1.pack(side = TOP, anchor = W, expand = YES)
    self.cont2 = Label(group62.interior(), text = 'Amor Ines, Ph.D. at Univ. of Minnesota and IRI', padx = 5, pady = 5)
    self.cont2.pack(side = TOP, anchor = W, expand = YES)

