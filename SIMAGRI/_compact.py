# -*- coding: utf-8 -*-

import logging
import sys

LOGGER = logging.getLogger('SIMAGRI')

# Import Pmw from this directory tree.
sys.path[:0] = ['../../..']

from tkinter import *
import tkinter #Tkinter is the PYthon interface to Tk, the GUI toolkit for Tcl/Tk
import tkinter.filedialog
from tkinter.filedialog import askdirectory
from tkinter import Label
import tkinter.messagebox   #tkMessageBox(2.7) → tkinter.messagebox (3.x)
import Pmw #Pmw(Python megawidgets) are composite widgets written entirely in Python using Tkinter widgets as base classes
       #Pmw provide a convinent ways to add functionality to an appilcation without the need to writ ea lot of code (e.g., Combobox) 
import datetime    #to convert date to doy or vice versa
import subprocess  #to run executable
import shutil   #to remove a foler which is not empty
import os   #operating system
from os import path # path
import calendar
import pandas as pd
import math
import scipy
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
from scipy.optimize import curve_fit
import time

import numpy as np
import matplotlib.pyplot as plt  #to create plots
import fnmatch   # Unix filename pattern matching => to remove PILI0*.WTD

import traceback

import csv

from scipy.stats import rankdata  #to make a rank to create yield exceedance curve

# get current directory
CurCWD = os.getcwd()

PY2 = sys.version_info[0] == 2
if PY2:
    LOGGER.debug('Python 2.x')
    text_type = unicode  # noqa
    from StringIO import StringIO

else:
    LOGGER.debug('Python 3.x')
    text_type = str
    from io import StringIO

# check if windows OS
import platform
WindowsOS = platform.system().lower().startswith('win')


# for using uivar variables in all UIs
from SIMAGRI._setting import Setting
from SIMAGRI.util import SIMAGRIUtil, AutoDetectUtil


# resources localization for i18n (internationalization)
language = "en"
if language == "en":
  from SIMAGRI.resources import localization_en as loc
else:
  from SIMAGRI.resources import localization_en as loc
