# -*- coding: utf-8 -*-

'''
TODO:
'''

SIMAGRI_Program_Name = 'Climate-Agriculture Decision Support System (Ethiopia)'
SIMAGRI_MAIN_UI_Title = SIMAGRI_Program_Name + ' User-Interface'


class Main:
  Load_Configuration = 'Do you want to load a SIMAGRI configuration file?'
  Save_Configuration = 'Do you want to save a SIMAGRI configuration file?'

class SimulationSetup:
  Title = "Simulation setup"
  Sim_Harvesting_Year = "Harvesting Year(4digit):"
  Sim_Harvesting_Month = "Harvesting Month:"

class TemporalDownscaling:
  Title = "Temporal Downscaling"


class DSSATSetup1:
  Title = "DSSAT setup 1"


class DSSATSetup2:
  Title = "DSSAT setup 2"


# class DISEASESetup:
#   Title = "DISEASE setup"

#   MSG_Run_DISEASE_Model = "Run DISEASE model ?"


class ScenariosSetup:
  Title = "*Scenarios setup"
  MSG_Click_Select_Working_Directory = '(1) Click to select a working directory'
  MSG_Copy_SIMAGRI_module_files = '(2) Copy SIMAGRI module files to working directory'
  MSG_Note_Working_Directory1 = '*NOTE: Make sure all input files are in the selected directory'
  MSG_Note_Working_Directory2 = '          Output files will be created under the selected directory with new scenario names'

class Credit:
  Title = "*CREDIT"
  Authors = "Authors:"
  Other_Contributions = "Other Contributors:"


class Msg:
  Not_added = 'Not added'
  Not_added_abbr = 'N/A'
