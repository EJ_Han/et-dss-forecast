# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 14:32:10 2016

@author: user
"""

import os

all_path = os.environ.get('path').split(';')

rscript_run_file = 'Rscript.exe'

rscript_run_path = None

for tmppath in all_path:
  if(os.path.isfile(os.path.join(tmppath, rscript_run_file))):
    rscript_run_path = tmppath


# in Program Files folder
if rscript_run_path is None:

  all_path = os.environ.get('programfiles').split(';')
  
  for tmppath in all_path:
    for dirpath in os.listdir(os.path.join(tmppath, 'R')):
      if (os.path.isfile(os.path.join(tmppath, 'R', dirpath, 'bin', rscript_run_file))):
        rscript_run_path = os.path.join(tmppath, 'R', dirpath, 'bin', rscript_run_file)
        break
    
    if rscript_run_path is None:
      break
      
# in Program Files (x86) folder
if rscript_run_path is None:

  all_path = os.environ.get('programfiles(x86)')
  
  for tmppath in all_path:
    for dirpath in os.listdir(os.path.join(tmppath, 'R')):
      if (os.path.isfile(os.path.join(tmppath, 'R', dirpath, 'bin', rscript_run_file))):
        rscript_run_path = os.path.join(tmppath, 'R', dirpath, 'bin', rscript_run_file)
        break
    
    if rscript_run_path is None:
      break

print rscript_run_path
